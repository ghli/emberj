/////////////////////////////
// Main.java
// 5-9-99
//
/**
 *  The main class for
 *  starting DesktopManager (EmberJ)
 *  @author Stephen C. Ferguson
 *  @version 1e-32
 *  @parm args String[] is ignored.
 *  @return void
 *  @see emberj.main
 *
 *  Copyright 1999.
 *  Send e-mail to stephenf@hiwaay.net
 *  web page: http://home.hiwaay.net
 *
 *  This software is freeware
 *  for non-commercial use.
 *
 *  COMPILATION INSTRUCTIONS  
 *     UNIX WINDOWS 95/98/NT:
 *       javac Main.java * /*.java
 *           "remove space^"
 *       java Main
 *     
 */

import emberj.*;
import java.applet.*; 

public class Main extends Applet 
{
	/**
		@args - unprocessed
	*/
	
	static String title[] = {""};
	
	public void init() 
	{
	 	DesktopManager.main(title);
  }

	public static void main(String args[]) 
	{
		DesktopManager.main(title);
  }		
}

