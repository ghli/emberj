///////////////////////////////////
// color02Action

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class color02Action extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // color02Action, constructor.
  // Set the name and icon for this action.       
  public color02Action(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "02.) coloring method" );

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "coloring02, ..." );
    		
		Mandelbrot.coloring = 2;
		Mandelbrot.draw_the_image();							
  }            

    
}