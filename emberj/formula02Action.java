///////////////////////////////////
// formula02Action

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class formula02Action extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // formula02Action, constructor.
  // Set the name and icon for this action.       
  public formula02Action(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "02.) init z=1; z=z-((z*z*z-1)/(3*z*z))+c [3rd order Nova]");

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "formula02, ..." );
    		
		Mandelbrot.formula = 2;
		Mandelbrot.draw_the_image();							
  }                
}

