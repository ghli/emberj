///////////////////////////////////
// render02Action

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class render02Action extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // render02Action, constructor.
  // Set the name and icon for this action.       
  public render02Action(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "02.) Last z values" );

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "render01, ..." );
    		
		//Mandelbrot.ResetCoordinates();
		Mandelbrot.render = 2;
		Mandelbrot.draw_the_image();							
  }            

    
}