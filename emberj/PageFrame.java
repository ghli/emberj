///////////////////////////////////////////////////
// PageFrame.java
// A simple extension of the JInternalFrame class

package emberj;
 
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PageFrame extends JInternalFrame
{
  DesktopManager dm;
  Mandelbrot Mandelbrot;
  JLabel pressedX, pressedY, releasedX, releasedY;
  JLabel sizeX, sizeY;

	Icon icon;
	JLabel lab;
	Container contentPane;
	JScrollPane scrollPane;
	JPanel panelSouth;
				  
  //int image_width, image_height;

  public PageFrame(String name, DesktopManager DM) 
  {
    super(name, true, false, true, true);
    dm = DM;
    setBounds(0, 0, DM.m_width+30, DM.m_height+80);    
    Mandelbrot = new Mandelbrot(this, DM);                        
    contentPane = getContentPane();                

		//////////////////////////////////////////////////////////////
		// set up the internal frame menu bar and actions
    JMenuBar jmb = new JMenuBar();

		//////////////////////////////////////////////////////////////
    JMenu fileMenu = new JMenu("File");

    Action FileSaveAsAction = new FileSaveAsAction(DM, this, Mandelbrot);            
    fileMenu.add(FileSaveAsAction);
    
    Action disposeAction = new DisposeInternalFrameAction(DM, this, Mandelbrot);            
    fileMenu.add(disposeAction);
    jmb.add(fileMenu);

		//////////////////////////////////////////////////////////////
    JMenu imageMenu = new JMenu("Image");

    Action resetAction = new ImageResetAction(DM, Mandelbrot);            
    imageMenu.add(resetAction);
    jmb.add(imageMenu);

		//////////////////////////////////////////////////////////////
    setJMenuBar(jmb);        
				
		//////////////////////////////////////////////////////////////
    Mandelbrot.init();
    Mandelbrot.start();    		        		
    
		set_image_container();
    
  }
    
	///////////////
	public void set_image_container()
	{    
		icon = new ImageIcon(Mandelbrot.image);        
		
		// add the image
		lab = new JLabel(icon);				

		/////////
		scrollPane = new JScrollPane(lab);
		contentPane.add( scrollPane, BorderLayout.CENTER);    									

		/*
		contentPane.add( panelSouth, BorderLayout.SOUTH);    
		*/
    
		///////	mouse event handler
		// The following works for mouse clicks, but not mouse movements
		lab.addMouseListener(new SelfMousePanel(this, lab, dm)); 				
		/////// end test
	}
}