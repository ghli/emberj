///////////////////////////////
// SelfMousePanel

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//class SelfMousePanel extends JPanel implements MouseListener, MouseMotionListener
class SelfMousePanel extends Canvas implements MouseListener, MouseMotionListener
{	
  private int startX, startY, endX, endY, offsetX, offsetY;
  Color color = Color.black;
	
	PageFrame parent;    
	JLabel lab;	
	DesktopManager dm;
  
  public SelfMousePanel(PageFrame iF, JLabel label, DesktopManager DM) 
  {
    //super (true); // double buffer
    //setOpaque (false);
    //setBorder (BorderFactory.createLoweredBevelBorder());
    addMouseListener (this);
    addMouseMotionListener (this);
    parent = iF;
    lab = label;
    dm = DM;
  }

  public void paint (Graphics g) 
  {
		JOptionPane.showMessageDialog( null , "paint");
    g.setColor (color);
    g.drawLine (startX, startY, endX, endY);       
  }

	public void calculate_offset()		
	{
	  Dimension r = new Dimension();
	  r = lab.getSize();
	  
    offsetX = (r.width  - dm.m_width)  / 2;
    offsetY = (r.height - dm.m_height) / 2;			
	}
  
  public void mousePressed(MouseEvent e) 
  {  	
		//JOptionPane.showMessageDialog( null , "mouse pressed");
				  	
    color = Color.red;
    startX = endX = e.getX();
    startY = endY = e.getY();
    repaint();
    
		calculate_offset();
    
    //parent.pressedX.setText(" pX: " + String.valueOf(startX - offsetX));
    //parent.pressedY.setText(" pY: " + String.valueOf(startY - offsetY));                               
    
  }
  
  public void mouseReleased(MouseEvent e) 
  {
		//JOptionPane.showMessageDialog( null , "mouse released");

    endX = e.getX();
    endY = e.getY();

    color = Color.black;
    repaint();
        
		calculate_offset();
        
    //parent.releasedX.setText(" rX: " + String.valueOf(endX - offsetX));
    //parent.releasedY.setText(" rX: " + String.valueOf(endY - offsetY));        
    
    //////////////////////////
    
    dm.pf.Mandelbrot.mouse_released((startX - offsetX), (startY - offsetY), endX, endY);    	            
  }
  
  public void mouseDragged(MouseEvent e) 
  {
	  JOptionPane.showMessageDialog( null , "mouse dragged");

    endX = e.getX();
    endY = e.getY();
    repaint();
  }
  
  public void mouseMoved(MouseEvent e) 
  {
	  JOptionPane.showMessageDialog( null , "mouse moved");
  }
  
  public void mouseClicked(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}

}

