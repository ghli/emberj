///////////////////////////////////////////////////
// ColorControlFrame.java
// A simple extension of the JInternalFrame class
 
package emberj;

import java.awt.*;
import java.awt.event.*;
import java.lang.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

//public class ColorControlFrame extends JInternalFrame
public class ColorControlFrame extends JDialog
{
  DesktopManager dm;

  JLabel textLabelrStart;
  JLabel textLabelgStart;
  JLabel textLabelbStart;
  
	JSlider sliderRedStart;
	JSlider sliderGrnStart;
	JSlider sliderBluStart;
	  
  public ColorControlFrame(String name, DesktopManager DM) 
  {
    //super(name, true, false, true, true);
    super(DM, false);
    dm = DM;
    //setBounds(380, 305, 400, 200);
    setBounds(560, 100, 190, 300);

    Container contentPane = getContentPane();    
    
		//////////////////////////////////////////////////////////////
		// set up the color control frame menu bar and actions
    //JMenuBar jmb = new JMenuBar();
    //JMenu fileMenu = new JMenu("Close");

    //Action disposeAction = new DisposeParameterFrameAction(dm, this);        
    
    //fileMenu.add(disposeAction);
    //jmb.add(fileMenu);

    //setJMenuBar(jmb);        

    // create horizontal sliders
    sliderRedStart = new JSlider(JSlider.HORIZONTAL, 0, 255, 25);
    sliderRedStart.putClientProperty("JSlider.isFilled", Boolean.TRUE);
    sliderRedStart.setMinorTickSpacing(10);
    sliderRedStart.setPaintTicks(true);
    sliderRedStart.setPaintLabels(false);
    sliderRedStart.setLabelTable(sliderRedStart.createStandardLabels(50));    
    sliderRedStart.addChangeListener(new SliderListener());

    sliderGrnStart = new JSlider(JSlider.HORIZONTAL, 0, 255, 25);
    sliderGrnStart.putClientProperty("JSlider.isFilled", Boolean.TRUE);
    sliderGrnStart.setMinorTickSpacing(10);
    sliderGrnStart.setPaintTicks(true);
    sliderGrnStart.setPaintLabels(false);
    sliderGrnStart.setLabelTable(sliderGrnStart.createStandardLabels(50));
    sliderGrnStart.addChangeListener(new SliderListener());

    sliderBluStart = new JSlider(JSlider.HORIZONTAL, 0, 255, 25);
    sliderBluStart.putClientProperty("JSlider.isFilled", Boolean.TRUE);
    sliderBluStart.setMinorTickSpacing(10);
    sliderBluStart.setPaintTicks(true);
    sliderBluStart.setPaintLabels(true);
    sliderBluStart.setLabelTable(sliderBluStart.createStandardLabels(50));
    sliderBluStart.addChangeListener(new SliderListener());
    
    // Create text labels
		textLabelrStart = new JLabel("");
		textLabelrStart.setBorder( BorderFactory.createEtchedBorder());  		 

		// add the gStart text label	
		textLabelgStart = new JLabel("");
		textLabelgStart.setBorder( BorderFactory.createEtchedBorder());  		 

		// add the bStart text label	
		textLabelbStart = new JLabel("");
		textLabelbStart.setBorder( BorderFactory.createEtchedBorder());  		 
    
		//////////////////////////////////////////////////////////////
		// Create a panel assign grid layout with X rows, Y columns
		JPanel panelNorth = new JPanel(new GridLayout(3, 1));
		panelNorth.setBorder( BorderFactory.createEtchedBorder());

		panelNorth.add(sliderRedStart);			
		panelNorth.add(sliderGrnStart);			
		panelNorth.add(sliderBluStart);			
		
		//////////////////////////////////////////////////////////////
		// Create a panel assign grid layout with X rows, Y columns
		JPanel panelSouth = new JPanel(new GridLayout(3, 1));
		panelSouth.setBorder( BorderFactory.createEtchedBorder());
		
		panelSouth.add(textLabelrStart);
		panelSouth.add(textLabelgStart);
		panelSouth.add(textLabelbStart);
		
		// add the panels to the content pane			    
    contentPane.add(panelNorth, BorderLayout.NORTH);              
    contentPane.add(panelSouth, BorderLayout.SOUTH);              
    
    textLabelUpdate();
    
    this.show();
    
  }        
    
	public void textLabelUpdate()
	{
    textLabelrStart.setText("rStart: " + String.valueOf(dm.pf.Mandelbrot.dRedStart));        				
    textLabelgStart.setText("gStart: " + String.valueOf(dm.pf.Mandelbrot.dGrnStart));        				
    textLabelbStart.setText("bStart: " + String.valueOf(dm.pf.Mandelbrot.dBluStart));        				
	}
	
	class SliderListener implements ChangeListener
	{
		public void stateChanged(ChangeEvent chngEvt)
		{
			
			dm.pf.Mandelbrot.dRedStart = sliderRedStart.getValue();
			dm.pf.Mandelbrot.dGrnStart = sliderGrnStart.getValue();
			dm.pf.Mandelbrot.dBluStart = sliderBluStart.getValue();
				
			textLabelUpdate();	
			dm.ipf.textLabelUpdate();	
			
		}					
	}  
}




