/////////////////////////
// ImageParameterAction

package emberj;

import java.awt.event.ActionEvent;
import javax.swing.*;

public class ImageParameterAction extends AbstractAction 
{
  DesktopManager parent;
  
  public ImageParameterAction(DesktopManager dm) 
  {
    //super("Parameters", new ImageIcon("images/new.gif"));
    super("Parameters");
    parent = dm;
  }

  public void actionPerformed(ActionEvent ae) 
  {
		//JOptionPane.showMessageDialog( null , "New, ..." );
		parent.addParameterFrame("");
  }
}