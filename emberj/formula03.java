/////////////////////////
// formula03

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.*;

public class formula03 extends FormulaComponent
{
	Mandelbrot m;

  private double zr, zi, cr, ci, LSum, LVal, P, Q, K;   
  private double tzr, tzi;  
  private double zr1, zi1;    
  private double dzr, dzi;    
	
	private double nr, ni;  // numerator for complex divide
	private double dr, di;  // denominator for complex divide

	public formula03()		
	{
		//JOptionPane.showMessageDialog( null , "formula02 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "formula02 init, ..." );	
		
		zr = m.zr;
		zi = m.zi;
		
		cr = m.cr ;
		ci = m.ci ;
		
	  P = 0.133;
	  K = P;
	  Q = K;
	  
		LSum = 1;
		LVal = 0;		
	}

	public void formula()
	{										
    //LSum := (LSum * (K-2*K*P));
    
    /*
    if ((m.Iteration & 0x02) == 0) 
		  K = Math.sin(cr+Math.sin(ci)) * cr;		  
		else
		  K = Math.sin(ci+Math.sin(cr)) * ci;		  			
		*/
		  
    if ((m.Iteration & 0x02) == 0) 
		  K = ci;		  			
		else
		  K = cr;		  
		  
		m.zr = LSum;
		m.zi = LSum;
		    		
    m.xtot = LSum;
    m.ytot = LSum;
		    		
    //P = K*P-K*P*P;    // derivative is P-2*K*P
    //LSum = LSum * (K-2*K*P);  // the derivative
				    
    P = K*P-K*P*P-K*P*P*P; 
   	LSum = LSum * 2 * (K-2*K*P-3*K*P*P);  // the derivative

    //LVal = LVal + Math.sqrt(Math.abs(m.zerotol+LSum));
        
    //m.zr = LVal;
    //m.zi = LVal;
    
    //m.cm.zr1 = LSum;
    //m.cm.zi1 = LSum;		

    m.cm.zr = LSum;
    m.cm.zi = LSum;				        
    
	}						
}
