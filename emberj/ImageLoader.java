///////////////////////////////////
// ImageLoader

package emberj;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.MediaTracker;

import java.io.IOException;
import java.net.URL;

import javax.swing.*;
import javax.swing.Icon;
import javax.swing.ImageIcon;

// A class containing static methods that can
// be used to load Images and Icons.
public class ImageLoader extends Component 
{
  private static ImageLoader imageLoader;

   // ImageLoader, constructor.
   // There should not be any instances of this
   // class created outside the private one used
   // in the class.
  private ImageLoader() 
  {
    super();
  }

  // loadImage
  // Load the image with the given name.
  // @param imageName - The name of the image to load.
  // @return - The Image for the image with the given name.
  // @see #loadIcon
  public static Image loadImage( String imageName )
      throws java.lang.InterruptedException 
  {
    //
    // get the image, and wait for it to be loaded.
    //
    URL url = Object.class.getResource( imageName );

    //
    // If the URL could not be located above, try
    // prepending a '/' to the image's ResourceName. This
    // will cause the search to begin at the top of the
    // CLASSPATH.
    //
    if( url == null )
       url = Object.class.getResource( "/" + imageName );
    
    if( url == null ) 
    {
		  JOptionPane.showMessageDialog( null , "error opening file: " +  imageName);				
    	
      RuntimeException e = new RuntimeException( "Image " + 
                               imageName + " not found" );
      e.printStackTrace();
      throw e;
    }

    //
    // Get the image and wait for it to be loaded.
    //
    Image image = Toolkit.getDefaultToolkit().getImage( url );
    MediaTracker tracker = new MediaTracker( imageLoader );
    tracker.addImage( image, 0 );
    tracker.waitForID( 0 );

    return( image );
  } // loadImage

  /**
   * loadIcon
   * <p>
   * Load the Icon with the given name.
   * <p>
   * @param imageName - The name of the image to load.
   * @return - The Icon for the image with the given name.
   * @see #loadImage
   **/
  public static Icon loadIcon( String imageName )
      throws java.lang.InterruptedException 
  {
    Image image = loadImage( imageName );
    return( new ImageIcon( image ) );
  } // loadIcon

  /**
   * static initialization.
   * <p>
   * Create an instance of this class that can be
   * used as the parameter to the MediaTracker while
   * loading images.
   **/
  static 
  {
    imageLoader = new ImageLoader();
  } // static

} // ImageLoader
	
	
	
	