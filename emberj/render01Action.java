///////////////////////////////////
// render01Action

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class render01Action extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // render01Action, constructor.
  // Set the name and icon for this action.       
  public render01Action(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "01.) iteration count" );

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "render01, ..." );
    		
		//Mandelbrot.ResetCoordinates();
		Mandelbrot.render = 1;
		Mandelbrot.draw_the_image();							
  }            

    
}
