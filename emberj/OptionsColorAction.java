/////////////////////////
// OptionsColorAction

package emberj;

import java.awt.event.ActionEvent;
import javax.swing.*;

public class OptionsColorAction extends AbstractAction 
{
  DesktopManager parent;
  
  public OptionsColorAction(DesktopManager dm) 
  {
    //super("Parameters", new ImageIcon("images/new.gif"));
    super("Color controls");
    parent = dm;
  }

  public void actionPerformed(ActionEvent ae) 
  {
		//JOptionPane.showMessageDialog( null , "color control" );
		parent.addColorControlFrame("");
  }
}

