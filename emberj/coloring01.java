/////////////////////////
// coloring01

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class coloring01 extends ColoringComponent
{
	Mandelbrot m;
	double r, g, b;
	int red, grn, blu;
	
	double t1, t2, t3;
	
	public coloring01()		
	{
		//JOptionPane.showMessageDialog( null , "coloring01 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "coloring01 init, ..." );	
	}

	public void coloring()
	{				
		if (m.render == 1)
		{		  
		  //r = m.Iteration;
		  //g = m.Iteration;
		  //b = m.Iteration;

		  r = Math.log(Math.abs(m.zr1));
		  g = Math.log(Math.abs(m.zi1));
		  b = (r+g)*0.5;
		  
		}
		else
		{					
			//JOptionPane.showMessageDialog( null , "01 coloring, ..." );		
	  	//r = m.xtot;
	  	//b = m.ytot;
	  	//g = Math.sqrt(10*r*r + 10*b*b);	  	
	  	//r = (g + r)/10;
	  	//b = (g + b)/10;
	  	//g = g/10;
	  	
	  	r = m.xtot;
	  	g = m.ytot;
	  	b = m.xtot;

			/*	  
	  	if (zsqrX < zsqrY)
	  		zsqrX = zsqrY;
	  	else
	  		zsqrY = zsqrX;	  	
	 	 	*/
		
			//zsqrX = 2 - 1/(0.5+zsqrX);
			//zsqrY = 2 - 1/(0.5+zsqrY);		

			//zsqrX = 2 - 1/(0.5-(Math.abs(minX)));
			//zsqrY = 2 - 1/(0.5-(Math.abs(minY)));		

	  	//g = 0.5*Math.abs(zsqrX + zsqrY);	  	
	  	//r = zsqrX + g;
	  	//b = zsqrY + g;
  	
    	r = r*360;
  	  g = g*360;
	    b = b*360;
    
		}
		
    red = (int) (m.dRedStart + r*m.dRedStep);
    grn = (int) (m.dGrnStart + g*m.dGrnStep);
    blu = (int) (m.dBluStart + b*m.dBluStep);
    
		/*    

    r = m.const2PI * (r/360.0);
    g = m.const2PI * (g/360.0);
    b = m.const2PI * (b/360.0);

		t1 = r;
		t2 = g;
		t3 = b;
		
    r =  Math.cos((t1+t3)/2);
    g =  Math.cos((t2+t3)/2);
    b =  Math.cos((t1+t2)/2);

		// gray		
    //r = (r+g+b)*0.33;
    //g = (b+g)*0.5;
    //b = (g+r)*0.5;
    
    
    red = (int) ((r*127)+127);
    grn = (int) ((g*127)+127);
    blu = (int) ((b*127)+127);    
    
    */
    

    if ((red & 0x1FF) > 0xFF)
    {
    	red = red & 0xFF;    	// mask the color
      red = ~ red;        	// Invert the color
    }

    if ((grn & 0x1FF) > 0xFF)
    {
    	grn = grn & 0xFF;    	// mask the color
      grn = ~ grn;        	// Invert the color
    }

    if ((blu & 0x1FF) > 0xFF)
    {
    	blu = blu & 0xFF;     	// mask the color
      blu = ~ blu;         	// Invert the color
    }

		/*      
		if (xtot > ytot)
  	{
  		// invert the color
  		red = ~red;
  		grn = ~grn;
  		blu = ~blu;
  	}
  	*/
    
    ////////////////////    
    
	  red = red & 0xFF;
	  grn = grn & 0xFF;
	  blu = blu & 0xFF;      	  	  

    red = (red << 16) & 0x00ff0000;  // shift left 16 places            
    grn = (grn << 8)  & 0x0000ff00;  // shift left 8 places
        
    m.color = red | grn | blu;
    
    m.pixels[m.index]    = m.code | m.color;    
    m.index+=1;
		
	}					
	
}