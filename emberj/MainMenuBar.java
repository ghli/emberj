////////////////////////////////////////
// MenuBar
//

package emberj;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class MainMenuBar
{
	DesktopManager parent;

	// Constructor	
	public MainMenuBar(DesktopManager desktop)
	{			
		parent = desktop;
	}
			
	// Create the menu for the frame.
	public JMenuBar createMainMenu() 
	{	
	  JMenuBar menuBar = new JMenuBar();
	
	  JMenu file = new JMenu( "File" );          
	  file.add( new FileNewAction(parent));

	  //file.add( new NewInternalFrameAction(parent));
	  //file.add( new OpenInternalFrameAction());
	  //file.add( new ExitAction() );

    file.add( new OpenInternalFrameAction());

	  menuBar.add( file );
	  
    JMenu image = new JMenu( "Image" );          
    image.add( new ImageParameterAction(parent));    
    menuBar.add( image );    
	  
	  JMenu help = new JMenu( "Help" );          
	  help.add( new HelpAboutAction(parent));    
	  menuBar.add( help );
	  
	  Vector frames = new Vector();	  
	  
	  frames.add(parent);
	  return( menuBar );
	}
}


  
  
  