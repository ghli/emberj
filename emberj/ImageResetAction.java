///////////////////////////////////
// ImageResetAction

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class ImageResetAction extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // ImageResetAction, constructor.
  // Set the name and icon for this action.       
  public ImageResetAction(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "Reset" );

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "Image reset, ..." );
    		
		Mandelbrot.ResetCoordinates();
		Mandelbrot.draw_the_image();							
  }            
}