///////////////////////
// ExitAction

package emberj;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
 
// Class ExitAction
// An action that shuts down the java virtual machine.
public class ExitAction extends AbstractAction 
{
  // The default name and Icon for the Action.
  private static final String DEFAULT_NAME = "Exit";
  private static final String DEFAULT_ICON_NAME = "emberj/images/exit.gif";
  private static Icon DEFAULT_ICON;
  
  // ExitAction, default constructor.
  // Create the Action with the default name and Icon.
  public ExitAction() 
  {
    this( DEFAULT_NAME, DEFAULT_ICON );
  } // ExitAction
  
  // ExitAction, constructor.
  // Create the Action with the given name and 
  public ExitAction( String name ) 
  {
    this( name, DEFAULT_ICON );
  } // ExitAction
  
  // ExitAction, constructor.
  // Create the Action with the given name and 
  // Icon.
  public ExitAction( String name, Icon icon ) 
  {
    super( name, icon );
  } // ExitAction
  
  // putValue, overriden from AbstractAction.
  // Guard against null values being added to the Action.
  public void putValue( String key, Object value ) 
  {
    if( value != null )
      super.putValue( key, value );
  }
  
  // actionPerformed, from ActionListener
  // Perform the action. Exit the JVM.
  public void actionPerformed( ActionEvent event ) 
  {
    System.exit(0);
  } // actionPerformed

  // static initialization. Load the default Icon.
  static 
  {
    try 
    {
      DEFAULT_ICON = ImageLoader.loadIcon( DEFAULT_ICON_NAME );
    }
    catch( InterruptedException ie ) 
    {
      System.err.println("Could not load ExitAction default Icon");
    }
  }
} // ExitAction
	
	
	
	