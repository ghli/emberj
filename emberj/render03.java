/////////////////////////
// render03.java
// 
// May 1999, developed by Stephen C. Ferguson
//

package emberj;

import java.awt.*;
import java.awt.event.*;
import java.math.*;
import javax.swing.*;

public class render03 extends RenderingComponent
{
	Mandelbrot m;
	QSortAlgorithm q;
	
	double xArray[], yArray[];
	
	double xtmp, ytmp;
	double temp;
	
	public render03()
	{
		//JOptionPane.showMessageDialog( null , "render03 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;					
		m.render = 3;
		
		q = new QSortAlgorithm();			
			
		//JOptionPane.showMessageDialog( null , "render03 init, ..." );
		
		xArray = new double[m.Maxit];
		yArray = new double[m.Maxit];		
	}

	public boolean filter()
	{	
		/*
		if (m.Iteration == 0)
		{
			m.xtot = 0;
			m.ytot = 0;
		}	
		*/	
		    
		//m.zsqrX = m.zr*m.zr;		
		//m.zsqrY = m.zi*m.zi;				
		
		/*
		if (Math.abs(m.zr/m.zi) < m.dStrands)
		  m.xtot = m.xtot * (1+(1 - Math.abs(m.zr/m.zi)/m.dStrands));  
		  
		if (Math.abs(m.zi/m.zr) < m.dStrands)
		  m.ytot = m.ytot * (1+(1 - Math.abs(m.zi/m.zr)/m.dStrands));  
		*/
		  		  		  
		xtmp = Math.abs(m.zr);
		ytmp = Math.abs(m.zi);

		//xtmp = m.zr;
		//ytmp = m.zi;

    //xtmp = 2-1/(0.5+ytmp*xtmp-ytmp-Math.sin(ytmp*ytmp));
    //ytmp = 2-1/(0.5+ytmp*xtmp-xtmp-Math.sin(xtmp*xtmp));

		//xtmp = Math.abs(m.zr);
		//ytmp = Math.abs(m.zi);
		
		//xtmp = m.zr*m.zr;
		//ytmp = m.zi*m.zi;

		switch (m.render03_option)			
		{		
			case 1:
				// not too bad 	
		    temp = xtmp*xtmp-ytmp-Math.sin(ytmp);
		    ytmp = ytmp*ytmp-xtmp-Math.sin(xtmp);
		    xtmp = temp;
		   	break;
		    
			case 2:	
				// this is OK
		    temp = xtmp+xtmp-ytmp-Math.sin(ytmp*ytmp);
		    ytmp = xtmp+xtmp-xtmp-Math.sin(xtmp*xtmp);
		    xtmp = temp;
		   	break;
	
			case 3:	
		    // OK 2
		    temp = ytmp-Math.sin(xtmp-Math.sin(ytmp));
		    ytmp = xtmp-Math.sin(ytmp-Math.sin(xtmp));
		    xtmp = temp;
		   	break;
		
			case 4:	
				// OK 3
		    temp = xtmp*xtmp-(ytmp*ytmp-Math.cos(xtmp));
		    ytmp = ytmp*ytmp-(xtmp*xtmp-Math.cos(ytmp));
		    xtmp = temp;
		   	break;
		
			case 5:	
				// ok 4
		    temp = (ytmp*ytmp-Math.cos(xtmp));
		    ytmp = (xtmp*xtmp-Math.cos(ytmp));
		    xtmp = temp;
		   	break;
		
			case 6:	
				// OK 5
		    //xtmp = (ytmp-xtmp-Math.cos(xtmp)-Math.cos(xtmp*xtmp));
		    //ytmp = (xtmp-ytmp-Math.cos(ytmp)-Math.cos(ytmp*ytmp));
		    temp = ytmp - xtmp - Math.sin(ytmp-Math.sin(ytmp*ytmp));
		   	ytmp = xtmp - ytmp - Math.sin(xtmp-Math.sin(xtmp*xtmp));
		   	xtmp = temp;
		   	break;
		
			case 7:	
				// Maybe
		    temp = (ytmp-xtmp-Math.cos(xtmp*xtmp-Math.cos(xtmp*xtmp)));
		    ytmp = (xtmp-ytmp-Math.cos(ytmp*ytmp-Math.cos(ytmp*ytmp)));
		    xtmp = temp;
		   	break;
	    
		}
		
		xtmp = Math.abs(xtmp);
		ytmp = Math.abs(ytmp);
    
    if (xtmp < m.dStrands)
      //m.xtot = m.xtot*1/(1+(1-xtmp/m.dStrands));
      m.xtot = m.xtot + (1-xtmp/m.dStrands);
    
    if (ytmp < m.dStrands)
      //m.ytot = m.ytot*1/(1+(1-ytmp/m.dStrands));    
      m.ytot = m.ytot + (1-ytmp/m.dStrands);
            
		//JOptionPane.showMessageDialog( null , "render03 init, ..." );
      
		/*
    if ((xtmp) < m.dStrands+Math.abs(m.cr))
		{
			//if (m.xtot == 0)
				//m.xtot = 1;
      m.xtot = 1/(1+m.xtot+(1-(xtmp)/(m.dStrands+Math.abs(m.cr))));
		}
    
    if ((ytmp) < m.dStrands+Math.abs(m.ci))
		{
			//if (m.ytot == 0)
				//m.ytot = 1;
      m.ytot = 1/(1+m.ytot+(1-(ytmp)/(m.dStrands+Math.abs(m.ci))));    
		}
		*/
		  					
		xArray[m.Iteration] = m.xtot;
		yArray[m.Iteration] = m.ytot;
		
		///////////////////////////////////////		

		m.cm.zdr = m.zr1 - m.cm.zr;  // complex subtract previous z from from current z
		m.cm.zdi = m.zi1 - m.cm.zi;
				
    if ((m.cm.z_sum_of_squares() > m.Bailout) ||
    		(m.cm.zd_sum_of_squares() < m.zerotol))   	
		{
  	 	return true;
		}
		else				
		{
	  	return false;
		}					  	  	
	}	
	
	public void post_filter()
	{
		//m.xtot = m.Iteration/(double)m.Maxit;
		//m.ytot = m.Iteration/(double)m.Maxit;				
				
		//q.quickSort(xArray, 0, m.Iteration-1); // sort to get the median
		//q.quickSort(yArray, 0, m.Iteration-1); 		
		
		if (m.Iteration > 0)
		{
			m.xtot = Math.abs(xArray[(int)((double)m.Iteration-1)]);			
			m.ytot = Math.abs(yArray[(int)((double)m.Iteration-1)]);
			m.xtot = Math.sqrt(m.xtot);
			m.ytot = Math.sqrt(m.ytot);
		}
		else
		{
			m.xtot = 0;				
			m.ytot = 0;
		}

		// debug
		//m.xtot = 1;				
		//m.ytot = 1;
				
		/*
		if (m.zsqr0 > m.Bailout)
		{
			m.xtot = 1/(1+m.zsqrX);
			m.ytot = 1/(1+m.zsqrY);
		}
		else
		{						
			m.xtot = 1/(1+m.xtot);
			m.ytot = 1/(1+m.ytot);
		}
		*/
	}				
}