/////////////////////////
// formula02

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class formula02 extends FormulaComponent
{
	Mandelbrot m;

  private double zr, zi, cr, ci;   
  private double tzr, tzi;  
  private double zr1, zi1;    
  private double dzr, dzi;    
	
	private double nr, ni;  // numerator for complex divide
	private double dr, di;  // denominator for complex divide

	public formula02()		
	{
		//JOptionPane.showMessageDialog( null , "formula02 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "formula02 init, ..." );	

		m.zr = 1;
		m.zi = 0;
		
		zr = m.zr;
		zi = m.zi;
		
		cr = m.cr;
		ci = m.ci;
		
	}

	public void formula()
	{								
		
		///////////////////////////////////////////////
		// z=z-((z*z*z-1)/(3*z*z))+c  3rd order Nova
		// z=z-numerator/denominator								
		// get the numerator        
		// numerator = z*z*z-1      	     	
  	//m.cm.z_mult_z();
  	//m.cm.z_mult_complex(m.zr, m.zi);
  	
  	m.cm.z_cubed();
  	m.cm.z_sub_real(1.0);      	
  	
  	m.cm.nr = m.cm.zr;  // numerator real
  	m.cm.ni = m.cm.zi;  // numerator imaginary

		// get the denominator        
		// denominator = 3*z*z      	     	
		m.cm.zr = m.zr;
		m.cm.zi = m.zi;      	
  	m.cm.z_mult_z();
  	m.cm.z_mul_real(3.0);
  	
  	m.cm.dr = m.cm.zr;  // denominator real
  	m.cm.di = m.cm.zi;  // denominator imaginary      	      	      	
  	      		
  	m.cm.z_divide();					      		
  	
		// z=z-numerator/denominator								
		m.cm.complex_sub_z(m.zr, m.zi);
  	      		
  	m.cm.z_add_c();

		///////////////////////////////////////////////
		// z=z-((z*z*z-1)/(3*z*z))+c  3rd order Nova
		// z=z-numerator/denominator								
		// get the numerator        
		// numerator = z*z*z-1      	     	

  	//m.cm.z_cubed();
  	//m.cm.z_sub_real(1.0);      	
  	//m.cm.nr = m.cm.zr;  // numerator real
  	//m.cm.ni = m.cm.zi;  // numerator imaginary
  		
  	//m.cm.nova3numerator();

		// get the denominator        
		// denominator = 3*z*z      	     	
		
		/*
		m.cm.zr = m.zr;
		m.cm.zi = m.zi;      	
  	m.cm.z_mult_z();
  	m.cm.z_mul_real(3.0);
  	
  	m.cm.dr = m.cm.zr;  // denominator real
  	m.cm.di = m.cm.zi;  // denominator imaginary      	      	      	
  	*/

		//m.cm.nova3denominator();
		  	      		
  	//m.cm.z_divide();					      		
  	  	
		// z=z-numerator/denominator								
		//m.cm.complex_sub_z(m.zr, m.zi);  	      		
  	//m.cm.z_add_c();
  	
  	//m.cm.nova3();  	  	
  	
		// z-((z*z*z-1)/(3*z*z))+c;

		// get numerator
		// z*z*z-1		  

		/*
    tzr = zr;
    tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  
    nr = zr1*tzr - zi1*tzi;
    ni = zr1*tzi + zi1*tzr;          	
    
  	nr = nr-1;

		// get denominator		
  	// 3*z*z;
    //tzr = zr;
    //tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  	
		dr = zr1*3;
		di = zi1*3;  	
		
		//z_divide();
    dzr = (dr*nr + di*ni)/(dr*dr+di*di);
    dzi = (dr*ni - di*nr)/(dr*dr+di*di);		
				
		//complex_sub_z(m.zr, m.zi);  	      		
		zr = zr-dzr;
		zi = zi-dzi;
				
  	//z_add_c();
  	zr = zr + cr;
  	zi = zi + ci;
  					  			
  	// cleanup
  	m.cm.zr = zr;
  	m.cm.zi = zi;
  	
  	*/
  	
	}						
}

