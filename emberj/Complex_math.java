////////////////////////////
// Complex_math.java
//

package emberj;

public class Complex_math
{
  public double zr, zi, cr, ci;   
  private double zr1, zi1;  
  private double tzr, tzi;  
  private double dzr, dzi;    
  public double zdr, zdi;    
    
	double nr, ni;  // numerator for complex divide
	double dr, di;  // denominator for complex divide
	
  public Complex_math(double zr, double zi, double cr, double ci) 
  {
    this.zr = zr;
    this.zi = zi;
    this.cr = cr;
    this.ci = ci;    
  }

  public Complex_math() 
  {
  }

  public double z_real() 
  {
  	return zr; 
  }

  public double z_imag() 
  {
  	return zi; 
  }

  public double c_real() 
  {
  	return cr; 
  }

  public double c_imag() 
  {
  	return ci; 
  }

  public double z_magnitude() 
  {
  	return Math.sqrt(zr*zr + zi*zi); 
  }
  
  public double z_sum_of_squares() 
  {
  	return (zr*zr + zi*zi); 
  }  
  
  public double zd_sum_of_squares() 
  {
  	return (zdr*zdr + zdi*zdi); 
  }    

	// add z=z+z  
  public void z_add_z() 
  {
    // z = z*z;
    // r+i = (r+i)+(r+i)
  	
  	zr = zr + zr;
  	zi = zi + zi;
  }    

	// add z=z+c  
  public void z_add_c() 
  {
  	zr = zr + cr;
  	zi = zi + ci;
  }    

  // A static class method to multiply complex numbers 
  //public static Complex mult(Complex a, Complex b) 
  
  public void z_mult_z()
  {
    //return new Complex(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);    
        
    tzr = zr;
    tzi = zi;
    
    zr = tzr*tzr - tzi*tzi;
    zi = tzr*tzi + tzi*tzr;
    
  }    
  
  public void z_cubed()
  {
    //return new Complex(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);    
        
    tzr = zr;
    tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
    
    zr = zr1*tzr - zi1*tzi;
    zi = zr1*tzi + zi1*tzr;        
  }    
  
	public void nova3numerator()
	{  
		/*
  	m.cm.z_cubed();
  	m.cm.z_sub_real(1.0);      	
  	m.cm.nr = m.cm.zr;  // numerator real
  	m.cm.ni = m.cm.zi;  // numerator imaginary
  	*/

		// z*z*z-1		  
    tzr = zr;
    tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  
    nr = zr1*tzr - zi1*tzi;
    ni = zr1*tzi + zi1*tzr;          	
    
  	nr = nr-1;
      	
	}
  
  public void nova3denominator()
	{
		/*		
		m.cm.zr = m.zr;
		m.cm.zi = m.zi;      	
  	m.cm.z_mult_z();
  	m.cm.z_mul_real(3.0);
  	
  	m.cm.dr = m.cm.zr;  // denominator real
  	m.cm.di = m.cm.zi;  // denominator imaginary      	      	      	
  	*/
  	
  	// 3*z*z;
    tzr = zr;
    tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  	
		dr = zr1*3;
		di = zi1*3;  	
		
	}
    
  public void nova3()
	{
		// z-((z*z*z-1)/(3*z*z))+c;

		// get numerator
		// z*z*z-1		  
    tzr = zr;
    tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  
    nr = zr1*tzr - zi1*tzi;
    ni = zr1*tzi + zi1*tzr;          	
    
  	nr = nr-1;

		// get denominator		
  	// 3*z*z;
    //tzr = zr;
    //tzi = zi;
    
    zr1 = tzr*tzr - tzi*tzi;
    zi1 = tzr*tzi + tzi*tzr;
  	
		dr = zr1*3;
		di = zi1*3;  	
		
		//z_divide();
    dzr = (dr*nr + di*ni)/(dr*dr+di*di);
    dzi = (dr*ni - di*nr)/(dr*dr+di*di);		
				
		//complex_sub_z(m.zr, m.zi);  	      		
		zr = zr-dzr;
		zi = zi-dzi;
				
  	//z_add_c();
  	zr = zr + cr;
  	zi = zi + ci;
				
	}  
    
  public void z_mult_complex(double x, double y)
  {
    //return new Complex(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);    
    
    // z = z*z;
    // r+i = (r+i)*(r+i)
    // r+i = r*r + r*i + i*r + i*i;
    // r+i = r*r + 2*(r*i) + i*i;
    
    tzr = zr;
    tzi = zi;
    
    zr = tzr*x - tzi*y;
    zi = tzr*y + tzi*x;
    
  }    
    
	// add_real z=z+real  
  public void z_add_real(double real) 
  {  
		//inline friend cmplx &operator+(cmplx &num,double real)
	    //     {return cmplx(num.x+real,num.y);}

		zr = zr+real;
  }			

	// subtract_real z=z-real  
  public void z_sub_real(double real) 
  {  
		//inline friend cmplx &operator-(cmplx &num,double real)
	    //     {return cmplx(num.x-real,num.y);}

		zr = zr-real;
  }			

	// subtract complex from z
  public void complex_sub_z(double x, double y) 
  {  
    //inline cmplx& operator-(cmplx& v) {return cmplx(x-v.x, y-v.y);} // subtract

		zr = x-zr;
		zi = y-zi;
  }			
    
	// multiply_real z=z*real  
  public void z_mul_real(double real) 
  {  
		//inline friend cmplx &operator*(cmplx &num,double real)
	    //     {return cmplx(num.x*real,num.y*real);}

		zr = zr*real;
		zi = zi*real;
  }			
  
	// divide_real z=z*real  
  public void z_div_real(double real) 
  {  
		//inline friend cmplx &operator/(cmplx &num, double real)
	    //     {return cmplx(num.x/real,num.y/real);}  

		zr = zr/real;
		zi = zi/real;	    	    
  }			              
    
  public void z_divide()
  {      
		/*  
	  inline cmplx& operator/(cmplx& v)
		{ 
			return cmplx((v.x*x+v.y*y)/(v.x*v.x+v.y*v.y),
	               (v.x*y-v.y*x)/(v.x*v.x+v.y*v.y));  // divide
	  }
	  */      
        
    //zr = (nr*dr + ni*di)/(nr*nr+ni*ni);
    //zi = (nr*di - ni*dr)/(nr*nr+ni*ni);

    zr = (dr*nr + di*ni)/(dr*dr+di*di);
    zi = (dr*ni - di*nr)/(dr*dr+di*di);
    
  }    
  
  
}