/////////////////////////
// render01

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class render01 extends RenderingComponent
{
	Mandelbrot m;
	double temp;	
	
	public render01()		
	{
		//JOptionPane.showMessageDialog( null , "render01 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		m.render = 1;
	}

	public boolean filter()
	{
		//JOptionPane.showMessageDialog( null , "render01 filter, ..." );		
		
    //zsqrX = zr*zr;
    //zsqrY = zi*zi;
    
    //zsqrX = (zr*zr)/Math.abs(zi);
    //zsqrY = (zi*zi)/Math.abs(zr);                

    //zsqrX = (zr*zr)/Math.abs(zi)+Bailout;
    //zsqrY = (zi*zi)/Math.abs(zr)+Bailout;

    //zsqrX = Bailout+Math.sin(zr*zr*100);
    //zsqrY = Bailout+Math.sin(zi*zi*100);
    
    //zsqrX = (zsqrX + Math.abs(zi*zi*100))/2;
    //zsqrY = (zsqrY + Math.abs(zr*zr*100))/2;
    
    //zsqrX = m.zr*m.zr+Bailout;
    //zsqrY = m.zi*m.zi+Bailout;

    //zsqrX = m.zr+Bailout;
    //zsqrY = m.zi+Bailout;
    
    //zsqrX = Math.abs(zsqrX-zsqrCX);
    //zsqrY = Math.abs(zsqrY-zsqrCY);

    //zsqrX = Math.abs(Math.sin(zsqrX*100));
    //zsqrY = Math.abs(Math.sin(zsqrY*100));               

    //if (minX > zsqrX)
      //minX = zsqrX;
      
    //if (minY > zsqrY)
      //minY = zsqrY;        

    //zsqr0 = zsqrX + zsqrY;                

    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zsqr0 > Bailout))

    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || 
    	//      (zsqrX/(zsqrY) > Bailout) ||
    	  //    (zsqrY/(zsqrX) > Bailout))  // biomorph method
    	
    //if ((zsqrX/(Bailout+zsqrY) > Bailout) ||
    	//  (zsqrY/(Bailout+zsqrX) > Bailout))  // biomorph by division method

    //if ((zsqrX > Bailout) ||
    //	  (zsqrY > Bailout))
        //(zsqrX < zerotol) ||
    	  //(zsqrY < zerotol))    // biomorph method        	

    //if ((zsqrX+zsqrY) > Bailout)
			//break;

    //if ((Math.abs(zsqr0-zsqr1) < zerotol))
			//break;
			
    //minX = minX + zsqrX;
    //minY = minY + zsqrY;        					
			
		//zsqr1 = zsqr0;
	      	
  	///////////////////////////
  	
  	//z = z.mult(z);
  	//z = z.add(c);
  	
  	//zx = z.real();
  	//zy = z.imag();
  	//cx = c.real();
  	//cy = c.imag();      	
  	
  	//temp = z.sum_of_squares();
  	////////////////////////////////
  	
    //tzr = zx*zx - zy*zy + cx;
    //tzi = 2*zx*zy + cy;

    //zx = tzr;
    //zy = tzi;
            
    //zsqrX = zr*zr;
    //zsqrY = zi*zi;
    //zsqr0 = zsqrX + zsqrY;                
    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zr > 4) || (zi > 4))  // biomorph method
    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zr > 4) || (zi > 4))  // biomorph method

		//temp = zx*zx+zy*zy;
		
  	//if (temp > Bailout)
      //break;        

		/*      	      	      	
  	if (temp < Bailout)
  		System.out.println("Iteration = " + Iteration);
  	else
  		break;
  	*/
		
		m.cm.zdr = m.zr1 - m.cm.zr;  // complex subtract previous z from from current z
		m.cm.zdi = m.zi1 - m.cm.zi;
				
    if ((m.cm.z_sum_of_squares() > m.Bailout) ||
    		(m.cm.zd_sum_of_squares() < m.zerotol))   	
		{
  	 	return true;
		}
		else				
		{
	  	return false;
		}
						
	}	
	
	public void post_filter()
	{
		//JOptionPane.showMessageDialog( null , "render01 post filter, ..." );
					
		m.xtot = m.Iteration;
		m.ytot = m.Iteration;
			
	}					
	
}