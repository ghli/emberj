/////////////////////////
// render02

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class render02 extends RenderingComponent
{
	Mandelbrot m;

	QSortAlgorithm q;
	double xArray[], yArray[];	
	double xtmp, ytmp;
	double temp;
	
	public render02()
	{
		//JOptionPane.showMessageDialog( null , "render02 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		m.render = 2;
		
		q = new QSortAlgorithm();			
			
		//JOptionPane.showMessageDialog( null , "render03 init, ..." );
		
		xArray = new double[m.Maxit];
		yArray = new double[m.Maxit];		
	}

	public boolean filter()
	{	
		if (m.Iteration == 0)
		{
			m.xtot = 1;
			m.ytot = 1;				
		}
		
		xtmp = Math.abs(m.zr);
		ytmp = Math.abs(m.zi);		
		
    m.xtot = m.zsqrX = m.zr*m.zr;
    m.ytot = m.zsqrY = m.zi*m.zi;
    
    //m.xtot = m.zr*m.zr;
    //m.ytot = m.zi*m.zi;

    //m.xtot = Math.abs(Math.sin(m.zr))+Math.abs(Math.cos(m.zi));
    //m.ytot = Math.abs(Math.sin(m.zi))+Math.abs(Math.cos(m.zr));                

    //m.xtot = (m.zr*m.zr)/ytmp;
    //m.ytot = (m.zi*m.zi)/xtmp;
    
    //m.xtot = m.zsqrX/m.zsqrY;
    //m.ytot = m.zsqrY/m.zsqrX;

    //m.xtot = 1/(1+Math.abs(m.xtot-Math.abs(Math.sin(m.zr))));
    //m.ytot = 1/(1+Math.abs(m.ytot-Math.abs(Math.sin(m.zi))));
    
    //zsqrX = (zsqrX + Math.abs(zi*zi*100))/2;
    //zsqrY = (zsqrY + Math.abs(zr*zr*100))/2;
        
    //m.xtot = 1/(1+Math.abs(m.xtot-Math.abs(Math.sin(m.zr))));
    //m.ytot = 1/(1+Math.abs(m.ytot-Math.abs(Math.sin(m.zi))));    

    //zsqrX = m.zr+Bailout;
    //zsqrY = m.zi+Bailout;
    
    //zsqrX = Math.abs(zsqrX-zsqrCX);
    //zsqrY = Math.abs(zsqrY-zsqrCY);

    //m.xtot = Math.abs(Math.sin(m.zsqrX*100));
    //m.ytot = Math.abs(Math.sin(m.zsqrY*100));               

    //if (minX > zsqrX)
      //minX = zsqrX;
      
    //if (minY > zsqrY)
      //minY = zsqrY;        

    //zsqr0 = zsqrX + zsqrY;                

    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zsqr0 > Bailout))

    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || 
    	//      (zsqrX/(zsqrY) > Bailout) ||
    	  //    (zsqrY/(zsqrX) > Bailout))  // biomorph method
    	
    //if ((zsqrX/(Bailout+zsqrY) > Bailout) ||
    	//  (zsqrY/(Bailout+zsqrX) > Bailout))  // biomorph by division method

    //if ((zsqrX > Bailout) ||
    //	  (zsqrY > Bailout))
        //(zsqrX < zerotol) ||
    	  //(zsqrY < zerotol))    // biomorph method        	

    //if ((zsqrX+zsqrY) > Bailout)
			//break;

    //if ((Math.abs(zsqr0-zsqr1) < zerotol))
			//break;
			
    //minX = minX + zsqrX;
    //minY = minY + zsqrY;        					
			
		//zsqr1 = zsqr0;
	      	
  	///////////////////////////
  	
  	//z = z.mult(z);
  	//z = z.add(c);
  	
  	//zx = z.real();
  	//zy = z.imag();
  	//cx = c.real();
  	//cy = c.imag();      	
  	
  	//temp = z.sum_of_squares();
  	////////////////////////////////
  	
    //tzr = zx*zx - zy*zy + cx;
    //tzi = 2*zx*zy + cy;

    //zx = tzr;
    //zy = tzi;
            
    //zsqrX = zr*zr;
    //zsqrY = zi*zi;
    //zsqr0 = zsqrX + zsqrY;                
    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zr > 4) || (zi > 4))  // biomorph method
    //if ((Math.abs(zsqr0-zsqr1) < zerotol) || (zr > 4) || (zi > 4))  // biomorph method

		//temp = zx*zx+zy*zy;
		
  	//if (temp > Bailout)
      //break;        

		/*      	      	      	
  	if (temp < Bailout)
  		System.out.println("Iteration = " + Iteration);
  	else
  		break;
  	*/

		//m.zsqrX = m.zr*m.zr;
		//m.zsqrY = m.zi*m.zi;		

  	m.zsqr0 = m.zsqrX+m.zsqrY;

		//m.zsqr0 = m.zsqr0+Math.sin(m.zsqr0*100);
		
    m.xtot = (m.zsqrX+Math.sin(m.zsqrX*10))/2;
    m.ytot = (m.zsqrY+Math.sin(m.zsqrY*10))/2;

    //m.xtot = 10*Math.abs(m.zsqrX-(Math.sin(m.zsqrX*100)));
    //m.ytot = 10*Math.abs(m.zsqrY-(Math.sin(m.zsqrY*100)));
    
    //m.xtot = m.zsqrX;
    //m.ytot = m.zsqrY;
      	  	
    //if ((m.xtot/(m.ytot) > m.Bailout) ||
    	//  (m.ytot/(m.xtot) > m.Bailout))  // biomorph by division method

    //if ((Math.abs(m.zsqr0-m.zsqr1) < m.zerotol) || 
    	//				//m.zsqr0 > m.Bailout) ||
    					
		//if ((m.zsqrX/(1+m.zsqrY) > m.Bailout) ||
    	//  (m.zsqrY/(1+m.zsqrX) > m.Bailout))  // biomorph by division method
    	
    //if ((Math.abs(m.zsqr0-m.zsqr1) < m.zerotol) ||
    	//   (m.zsqr0 > 10+m.Bailout))

		xArray[m.Iteration] = m.xtot;
		yArray[m.Iteration] = m.ytot;

		/*
    if ((Math.abs(m.zsqr0+m.zsqr1) < m.zerotol) || 
    	       //(m.zsqr0 > m.Bailout) ||
             (m.zsqrX/(m.Bailout+m.zsqrY) > 10+m.Bailout) ||
    	       (m.zsqrY/(m.Bailout+m.zsqrX) > 10+m.Bailout))  // biomorph by division method
 		{
  	  m.zsqr1 = m.zsqr0;
  	 	return true;
		}
		else				
		{
  	  m.zsqr1 = m.zsqr0;
	  	return false;
		}
		*/
		
		
		m.cm.zdr = m.zr1 - m.cm.zr;  // complex subtract previous z from from current z
		m.cm.zdi = m.zi1 - m.cm.zi;
				
    if ((m.cm.z_sum_of_squares() > m.Bailout) ||
    		(m.cm.zd_sum_of_squares() < m.zerotol))   	
		{
  	 	return true;
		}
		else				
		{
	  	return false;
		}			  	  	
	}	
	
	public void post_filter()
	{
		//m.xtot = m.Iteration/(double)m.Maxit;
		//m.ytot = m.Iteration/(double)m.Maxit;				

		//q.quickSort(xArray, 0, m.Iteration-1); // sort to get the median
		//q.quickSort(yArray, 0, m.Iteration-1); 		

		//m.xtot = m.zsqrX;
		//m.ytot = m.zsqrY;

		if (m.Iteration > 0)
		{
			xtmp = xArray[m.Iteration-1];
			ytmp = yArray[m.Iteration-1];
			
			m.xtot = 1/(1+Math.abs(xtmp));
			m.ytot = 1/(1+Math.abs(ytmp));			
			
		}
		else
		{
			m.xtot = 0;				
			m.ytot = 0;
		}

		/*
		if (m.zsqr0 > m.Bailout)
		{
			m.xtot = m.zsqrX;
			m.ytot = m.zsqrY;
		}
		else
		{						
			m.xtot = m.xtot;
			m.ytot = m.ytot;
		}
		*/
	}			
	
}