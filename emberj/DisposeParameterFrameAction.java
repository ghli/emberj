///////////////////////////////////
// DisposeParameterFrameAction

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class DisposeParameterFrameAction extends AbstractAction 
{
  // The internal pane to close.
  private JInternalFrame ipf;
	DesktopManager dm;
  
  // DisposeParemeterFrameAction, constructor.
  // Set the name and icon for this action.       
  public DisposeParameterFrameAction(DesktopManager DM, JInternalFrame IPF) 
  {
    super( "Close", new ImageIcon( "images/close.gif" ) );
    //super( "", new ImageIcon( "images/close.gif" ) );

   	ipf = IPF;
    dm = DM;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "M closing, ..." );    
    ipf.dispose();    
		dm.ipf = null;    					
  }            
}