////////////////////////////////////////////////
// ColoringComponent.java

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

abstract public class ColoringComponent
{	
  public static ColoringComponent create(String name) 
  {
    ColoringComponent coloring = null;
    try 
    {
      coloring = (ColoringComponent) Class.forName(name).newInstance(); 
  		//JOptionPane.showMessageDialog( null , "component create, ..." );
    } 
    catch (ClassNotFoundException e) 
    {
  		JOptionPane.showMessageDialog( null , name + " Class NotFound exception" );
    } 
    catch (IllegalAccessException e) 
    {
  		JOptionPane.showMessageDialog( null , "IllegalAccess exception" );
    } 
    catch (InstantiationException e) 
    {
  		JOptionPane.showMessageDialog( null , "Instantiation exception" );
    }
    catch (ClassCastException e) 
    {
  		JOptionPane.showMessageDialog( null , "ClassCast exception" );
    }
    return coloring;
  }
	
	public void init(Mandelbrot M)
	{
  	JOptionPane.showMessageDialog( null , "coloring component init" );
	}	   	                

	public void coloring()
	{
  	JOptionPane.showMessageDialog( null , "component color" );
	}	   	                	   	                
}

