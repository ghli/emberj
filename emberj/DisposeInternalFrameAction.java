///////////////////////////////////
// DisposeInternalFrameAction

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class DisposeInternalFrameAction extends AbstractAction 
{
  // The internal pane to close.
  private JInternalFrame internalFrame;
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // CloseInternalFrameAction, constructor.
  // Set the name and icon for this action.       
  public DisposeInternalFrameAction(DesktopManager DM, JInternalFrame iFrame, Mandelbrot M) 
  {
    super( "Close", new ImageIcon( "images/close.gif" ) );
    //super( "", new ImageIcon( "images/close.gif" ) );

   	internalFrame = iFrame;
    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {
		//JOptionPane.showMessageDialog( null , "M closing, ..." );
		if (Mandelbrot.thread != null)
			Mandelbrot.stop();
    
    internalFrame.dispose();    
		dm.pf = null;    
		
		//dm.pf.Mandelbrot.ResetCoordinates();
		//dm.pf.Mandelbrot.draw_the_image();				
			
  }            
}