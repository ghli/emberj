////////////////////////////////////////////////
// FormulaComponent.java

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

abstract public class FormulaComponent
{	
  public static FormulaComponent create(String name) 
  {
    FormulaComponent formula = null;
    try 
    {
      formula = (FormulaComponent) Class.forName(name).newInstance(); 
  		//JOptionPane.showMessageDialog( null , "component create, ..." );
    } 
    catch (ClassNotFoundException e) 
    {
  		JOptionPane.showMessageDialog( null , name + " Class NotFound exception" );
    } 
    catch (IllegalAccessException e) 
    {
  		JOptionPane.showMessageDialog( null , "IllegalAccess exception" );
    } 
    catch (InstantiationException e) 
    {
  		JOptionPane.showMessageDialog( null , "Instantiation exception" );
    }
    catch (ClassCastException e) 
    {
  		JOptionPane.showMessageDialog( null , "ClassCast exception" );
    }
    return formula;
  }
	
	public void init(Mandelbrot M)
	{
  	JOptionPane.showMessageDialog( null , "formula component init" );
	}	   	                

	public void formula()
	{
  	JOptionPane.showMessageDialog( null , "component formula" );
	}	   	                	   	                
}


