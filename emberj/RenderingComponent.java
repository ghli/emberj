////////////////////////////////////////////////
// RenderingComponent.java

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

abstract public class RenderingComponent
{	
  public static RenderingComponent create(String name) 
  {
    RenderingComponent rendering = null;
    try 
    {
      rendering = (RenderingComponent) Class.forName(name).newInstance(); 
  		//JOptionPane.showMessageDialog( null , "component create, ..." );
    } 
    catch (ClassNotFoundException e) 
    {
  		JOptionPane.showMessageDialog( null , name + " Class NotFound exception" );
    } 
    catch (IllegalAccessException e) 
    {
  		JOptionPane.showMessageDialog( null , "IllegalAccess exception" );
    } 
    catch (InstantiationException e) 
    {
  		JOptionPane.showMessageDialog( null , "Instantiation exception" );
    }
    catch (ClassCastException e) 
    {
  		JOptionPane.showMessageDialog( null , "ClassCast exception" );
    }
    return rendering;
  }
	
	public void init(Mandelbrot M)
	{
  	JOptionPane.showMessageDialog( null , "component init" );
	}	   	                

	public boolean filter()
	{
  	JOptionPane.showMessageDialog( null , "component filter" );
  	return false;
	}	   	                
	
	public void post_filter()
	{

	}
   	                
}


