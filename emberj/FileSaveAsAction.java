///////////////////////////////////
// FileSaveAsAction

package emberj;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class FileSaveAsAction extends AbstractAction 
{
  // The internal pane to close.
  private JInternalFrame internalFrame;
	DesktopManager dm;
	Mandelbrot Mandelbrot;
	
	public short r[][];
	public short g[][];
	public short b[][];  		
  
  // CloseInternalFrameAction, constructor.
  // Set the name and icon for this action.       
  public FileSaveAsAction(DesktopManager DM, JInternalFrame iFrame, Mandelbrot M) 
  {
    super( "Save As <filename>.PPM ", new ImageIcon( "images/save.gif" ) );
    //super( "", new ImageIcon( "images/close.gif" ) );

   	internalFrame = iFrame;
    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "closing constructor" );				      
  }
  
  // Perform the action, dispose of the internal frame.
  public void actionPerformed( ActionEvent e ) 
  {  	
		// Prepare to call savePPM by converting the image data to RGB 
				
		r = new short[dm.m_width][dm.m_height];
		g = new short[dm.m_width][dm.m_height];
		b = new short[dm.m_width][dm.m_height];
		
		try 
		{
			int index = 0;
			int rgb;
  		for ( int col = 0; col < dm.m_height ; col++ ) 
  		{  
       	for ( int row = 0; row < dm.m_width; row++ ) 
       	{  
	      	//ppmPixels[j++] = (byte)r[row][col];
	      	//ppmPixels[j++] = (byte)g[row][col];
	      	//ppmPixels[j++] = (byte)b[row][col];	      	
	      	
	      	/*
			    red = (red << 16) & 0x00ff0000;  // shift left 16 places            
			    grn = (grn << 8)  & 0x0000ff00;  // shift left 8 places
			        
			    color = red | grn | blu;
			    
			    iteration[index] = i;
			    pixels[index]    = code | color;
			   	*/
	      		      	
					rgb = Mandelbrot.pixels[index];			      		      
	      	r[row][col] = (short) ((rgb & 0xff0000) >> 16);
	      	g[row][col] = (short) ((rgb &   0xff00) >>  8);
	      	b[row][col] = (short)  (rgb &     0xff);
	      	index++;
		 		}
   		}
  	}
  	catch (Exception ex) 
  	{
  		ex.printStackTrace();
  	}				
  	
  	
		saveAsPPM();  	  	
		
		//JOptionPane.showMessageDialog( null , "Save As, ..." );
		
		//if (Mandelbrot.thread != null)
			//Mandelbrot.stop();
    
    //internalFrame.dispose();    
		//dm.pf = null;    
		
		//dm.pf.Mandelbrot.ResetCoordinates();
		//dm.pf.Mandelbrot.draw_the_image();				
			
  }            
  
	public void saveAsPPM() 
	{
		String fn = getSaveFileName("Save as PPM");
		if (fn == null) return;
		saveAsPPM(fn);	
	}
	
	public void saveAsPPM(String fn) 
	{
		WritePPM.doIt(r, g, b, fn);
		JOptionPane.showMessageDialog( null , "Saved file: " + fn);		
		
	}  
      
	private static Frame f = new Frame();
	public static String getSaveFileName(String prompt) 
	{
		FileDialog fd = new 
		FileDialog(f, prompt, FileDialog.SAVE);
		fd.setVisible(true);
		fd.setVisible(false);
		String fn=fd.getDirectory()+fd.getFile();
		if (fd.getFile() == null) return null; //usr canceled
		return fn;
	}  	
  
}