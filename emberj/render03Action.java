///////////////////////////////////
// render03Action

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// An Action that disposes of an internal frame.
public class render03Action extends AbstractAction 
{
	DesktopManager dm;
	Mandelbrot Mandelbrot;
  
  // render03Action, constructor.
  // Set the name and icon for this action.       
  public render03Action(DesktopManager DM, Mandelbrot M) 
  {
    //super( "Reset", new ImageIcon( "images/close.gif" ) );
    super( "03.) trig function orbit counting " );

    dm = DM;
    Mandelbrot = M;

		//JOptionPane.showMessageDialog( null , "render 3 constructor" );				      
  }
  
  // Perform the action
  public void actionPerformed( ActionEvent e ) 
  {
  	String s = "<unknown>";
  	
  	switch (Mandelbrot.render03_option)
  	{
  		case 1:
  			s = "01";
  			break;

  		case 2:
  			s = "02";
  			break;

  		case 3:
  			s = "03";
  			break;  		  			  		  		

  		case 4:
  			s = "04";
  			break;  		  			  		  		

  		case 5:
  			s = "05";
  			break;  		  			  		  		

  		case 6:
  			s = "06";
  			break;  		  			  		  		

  		case 7:
  			s = "07";
  			break;  		  			  		  		
  	}  		  		  		
  		
		//JOptionPane.showMessageDialog( null , "render03, ..." );
		Object option = JOptionPane.showInputDialog( dm , "Select a render03 option", "Render03",
			JOptionPane.QUESTION_MESSAGE, null, 
				new Object[] { "01","02","03","04","05","06","07" }, s);
			
		if (option != null)
  	{
		  s = (String) option;
		
			//JOptionPane.showMessageDialog( null , "render03_option = " + s );		
			
			if (s == "01")
				Mandelbrot.render03_option = 1;		
			else
			if (s == "02")
				Mandelbrot.render03_option = 2;		
			else
			if (s == "03")
				Mandelbrot.render03_option = 3;		
			else
			if (s == "04")
				Mandelbrot.render03_option = 4;		
			else
			if (s == "05")
				Mandelbrot.render03_option = 5;		
			else
			if (s == "06")
				Mandelbrot.render03_option = 6;		
			else
			if (s == "07")
				Mandelbrot.render03_option = 7;		
					    					
			Mandelbrot.render = 3;
			Mandelbrot.draw_the_image();
  	}
  	else
			JOptionPane.showMessageDialog( null , "render03_option = " + s );		
  		
  }            
    
}



