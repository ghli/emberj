/////////////////////////
// coloring03

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class coloring03 extends ColoringComponent
{
	Mandelbrot m;
	double r, g, b;
	int red, grn, blu, i, tmp;
	int redx, grnx, blux;
	int redy, grny, bluy;
	
	public coloring03()		
	{
		//JOptionPane.showMessageDialog( null , "coloring03 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "coloring03 init, ..." );	
	}

	public void coloring()
	{
		//i = Iteration*(768/Maxit);
		
		//i = (int) (Math.abs((xtot+ytot)/2)*dRedStart);		

		////////////////////////
		// process xtot
		//	
				
		if (m.render == 1)
		{		  
		  i = m.Iteration;
		}
		else
		{									
			m.xtot = m.xtot;
			m.ytot = m.ytot;
				
			i = (int) ((Math.abs(m.xtot))*767);
		}
		
		//i = (int) (Math.abs((m.xtot)/2)*767);		
  	
    if (i < 256)
      redx = i;        
    else
      redx = 255;
      
    if (i < 512 && i > 255)
    {
      grnx = i - 256;
    }
    else
    {
      if (i >= 512)
        grnx = 255;
      else
        grnx = 0;
    }
                
    if (i < 768 && i > 511)
      blux = i - 512;
    else
  	{
  		if (i >= 768)
      	blux = 255;        
      else
      	blux = 0;
  	}
      
		////////////////////////
		// process ytot
		//			      
		
		//i = (int) (Math.abs((m.ytot)/2)*767);		
		
		if (m.render == 1)
		{		  
		  i = m.Iteration;
		}
		else
		{									
			m.xtot = m.xtot;
			m.ytot = m.ytot;
						
			i = (int) ((Math.abs(m.ytot))*767);		
		}
  	
    if (i < 256)
      redy = i;        
    else
      redy = 255;
      
    if (i < 512 && i > 255)
    {
      grny = i - 256;
    }
    else
    {
      if (i >= 512)
        grny = 255;
      else
        grny = 0;
    }
                
    if (i < 768 && i > 511)
      bluy = i - 512;
    else
  	{
  		if (i >= 768)
      	bluy = 255;        
      else
      	bluy = 0;
  	}


    // try swapping colors around
    tmp = redy;
    redy = grny;
    grny = bluy;
    bluy = tmp;

		/*
    // try swapping colors around
    tmp = redx;
    redx = grnx;
    grnx = blux;
    blux = tmp;
   	*/

		/////////////////////////
		// merge the two together
		//
				
		red = (int)Math.abs(m.dRedStart + m.dRedStep*((double)redx - (double)redy));
		grn = (int)Math.abs(m.dGrnStart + m.dGrnStep*((double)grnx - (double)grny));
		blu = (int)Math.abs(m.dBluStart + m.dBluStep*((double)blux - (double)bluy));
      
		/////////////////////////

		//red = red + (int) m.dRedStart;
		//grn = grn + (int) m.dGrnStart;
		//blu = blu + (int) m.dBluStart;
            
    if ((red & 0x1FF) > 0xFF)
    {
    	red = red & 0xFF;    	// mask the color
      red = ~ red;        	// Invert the color
    }

    if ((grn & 0x1FF) > 0xFF)
    {
    	grn = grn & 0xFF;    	// mask the color
      grn = ~ grn;        	// Invert the color
    }

    if ((blu & 0x1FF) > 0xFF)
    {
    	blu = blu & 0xFF;     	// mask the color
      blu = ~ blu;         	// Invert the color
    }
    
	  red = red & 0xFF;
	  grn = grn & 0xFF;
	  blu = blu & 0xFF;      	  	  

    // try swapping colors around
    tmp = red;
    red = grn;
    grn = blu;
    blu = tmp;

		//grn = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((grn + red + blu)/3)));			
		//red = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((red + blu + grn)/3)));
		//blu = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((blu + red + grn)/3)));			

		// try some gray colorings
		//grn = Math.abs((grn + red + blu)/3);			
		//red = Math.abs((red + blu + grn)/3);
		//blu = Math.abs((blu + red + grn)/3);			

		/*
		if (xtot > ytot)
  	{
  		// invert the color
  		red = ~red;
  		grn = ~grn;
  		blu = ~blu;
  	}
  	*/
    
    ////////////////////    
    
	  //red = red & 0xFF;
	  //grn = grn & 0xFF;
	  //blu = blu & 0xFF;      	  	  

    red = (red << 16) & 0x00ff0000;  // shift left 16 places            
    grn = (grn << 8)  & 0x0000ff00;  // shift left 8 places
        
    m.color = red | grn | blu;
    
    m.pixels[m.index] = m.code | m.color;    
    m.index+=1;
	}						
}


