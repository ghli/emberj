/////////////////////////
// ApplyButtonAction

package emberj;

import java.awt.event.ActionEvent;
import java.lang.*;
import javax.swing.*;

public class ApplyButtonAction extends AbstractAction 
{
  DesktopManager dm;
	ImageParameterFrame ipf;
	
	Integer w, h, maxit;
	
	Double rStart, gStart, bStart, rStep, gStep, bStep, Bailout;
	
	int saved_width, saved_height;
	  
  public ApplyButtonAction(DesktopManager DM, ImageParameterFrame IPF) 
  {
    //super("New", new ImageIcon("images/new.gif"));
    super("Apply");
    dm = DM;
    ipf = IPF;
    
    saved_width  = dm.m_width;
    saved_height = dm.m_height;
  }

  public void actionPerformed(ActionEvent ae) 
  {
  	//int w = panelNorth.getText();
  	
		String s_width  = ipf.textFieldWidth.getText();
		String s_height = ipf.textFieldHeight.getText();
		String s_maxit  = ipf.textFieldMaxit.getText();
				
		String s_rStep  = ipf.textFieldrStep.getText();
		String s_gStep  = ipf.textFieldgStep.getText();
		String s_bStep  = ipf.textFieldbStep.getText();

		String s_Bailout  = ipf.textFieldBailout.getText();
				  			
		w  		= Integer.valueOf(s_width);
		h 		= Integer.valueOf(s_height);
		maxit = Integer.valueOf(s_maxit);
		
		rStep = Double.valueOf(s_rStep);
		gStep = Double.valueOf(s_gStep);
		bStep = Double.valueOf(s_bStep);

		Bailout = Double.valueOf(s_Bailout);
				
		dm.m_width  = w.intValue();
		dm.m_height = h.intValue();	
		dm.pf.Mandelbrot.Maxit = maxit.intValue();
				
		dm.pf.Mandelbrot.dRedStep = rStep.doubleValue();
		dm.pf.Mandelbrot.dGrnStep = gStep.doubleValue();
		dm.pf.Mandelbrot.dBluStep = bStep.doubleValue();			

		dm.pf.Mandelbrot.Bailout = Bailout.doubleValue();			
				
		//String str = "Width = " + s_width + " Height = " + s_height;
		//JOptionPane.showMessageDialog( null , str);
		
		dm.ipf.textLabelUpdate();						

  	dm.pf.Mandelbrot.image.flush();															

		if ((saved_width != dm.m_width) || (saved_height != dm.m_height))
  	{
  		dm.pf.Mandelbrot.stop();							

			saved_width  = dm.m_width;
			saved_height = dm.m_height;	

  		dm.pf.Mandelbrot.image_resize();							
  	}
  	else
  	{
  		dm.pf.Mandelbrot.draw_the_image();	
  	}
  }
}

