///////////////////////////////////////
// DesktopManager.java
// SCF, 5-2-99
//

package emberj;

//import java.util.*;

import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;

import java.io.File;
import java.util.Hashtable;
import java.util.Enumeration;
import java.beans.*;
import java.net.*;

//import java.awt.image.MemoryImageSource;

import javax.swing.filechooser.*;

public class DesktopManager extends JFrame 
{
  JLayeredPane desktop;
  Vector popups = new Vector();
  
  int m_width, m_height;
  
	ImageParameterFrame ipf;
	ColorControlFrame ccf;

	PageFrame pf;
	
	public ImageIcon jpgIcon;
  public ImageIcon gifIcon;
  public ImageIcon testIcon;
  public ImageIcon exitIcon;

  public DesktopManager() 
  {
    super("EmberJ");
    
		m_width  = 160;
		m_height = 120;
		    
    setSize(800, 600);
    addWindowListener(new BasicWindowMonitor());
    Container contentPane = getContentPane();    

    loadImages();
            
    // Add our LayeredPane object for the Internal frames
    desktop = new JDesktopPane();
    contentPane.add(desktop, BorderLayout.CENTER);        

		ipf = null;
		pf  = null;
		ccf = null;
		
    addPageFrame("");
    
		// create the main menu bar        
    JMenuBar menuBar = createMenu();
    setJMenuBar( menuBar );    

		// create the tool bar
    JToolBar toolBar = createToolBar();
    Container content = getContentPane();
    content.add( toolBar, BorderLayout.NORTH) ;

    this.show();
    addParameterFrame("");        		// !!!!
    //addColorControlFrame("");        	// !!!! 10-14	
        
    /*
    // set the default Plugable Look and Feel
  	String lnfName = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
    try 
    {
      UIManager.setLookAndFeel(lnfName);
      SwingUtilities.updateComponentTreeUI(this);
    }
    catch (UnsupportedLookAndFeelException ex1) 
    {
      System.err.println("Unsupported LookAndFeel: " + lnfName);
    }
    catch (ClassNotFoundException ex2) 
    {
      System.err.println("LookAndFeel class not found: " + lnfName);
    }
    catch (InstantiationException ex3) 
    {
      System.err.println("Could not load LookAndFeel: " + lnfName);
    }
    catch (IllegalAccessException ex4) 
    {
      System.err.println("Cannot use LookAndFeel: " + lnfName);
    }
    */
	
  }
  
  // Create the menu for the frame.
  protected JMenuBar createMenu() 
  {
    JMenuBar menuBar = new JMenuBar();
    
		//JOptionPane.showMessageDialog( null , "creating menu" );				    	    

		//////////////////////////////////////////////////////////////
    JMenu file = new JMenu( "File" );          
    
	  file.add( new FileNewAction(this, "New"));    
    file.add( new OpenInternalFrameAction("Open"));
    file.add( new ExitAction("Exit", exitIcon));
    menuBar.add( file );
        
		//////////////////////////////////////////////////////////////
    JMenu options = new JMenu( "Options" );          
    options.add( new ImageParameterAction(this));    
    options.add( new OptionsColorAction(this));    
    
    options.add( new JSeparator());    

    options.add( new LnFListener(this, "Motif"));
    options.add( new LnFListener(this, "Metal"));
    options.add( new LnFListener(this, "Windows"));  
              
    menuBar.add( options );    
        
		//////////////////////////////////////////////////////////////
    JMenu formulaMenu = new JMenu("Formula");

    Action formula01Action = new formula01Action(this, pf.Mandelbrot);            
    formulaMenu.add(formula01Action);

    Action formula02Action = new formula02Action(this, pf.Mandelbrot);            
    formulaMenu.add(formula02Action);

    Action formula03Action = new formula03Action(this, pf.Mandelbrot);            
    formulaMenu.add(formula03Action);

    menuBar.add(formulaMenu);	  
    	  
		//////////////////////////////////////////////////////////////
    JMenu colorMenu = new JMenu("Color");

    Action color01Action = new color01Action(this, pf.Mandelbrot);            
    colorMenu.add(color01Action);

    Action color02Action = new color02Action(this, pf.Mandelbrot);            
    colorMenu.add(color02Action);

    Action color03Action = new color03Action(this, pf.Mandelbrot);            
    colorMenu.add(color03Action);

    menuBar.add(colorMenu);	  
    
		//////////////////////////////////////////////////////////////
    JMenu renderMenu = new JMenu("Render");

    Action render01Action = new render01Action(this, pf.Mandelbrot);            
    renderMenu.add(render01Action);

    Action render02Action = new render02Action(this, pf.Mandelbrot);            
    renderMenu.add(render02Action);

    Action render03Action = new render03Action(this, pf.Mandelbrot);            
    renderMenu.add(render03Action);

    menuBar.add(renderMenu);	  
		//////////////////////////////////////////////////////////////
	  
	  JMenu help = new JMenu( "Help" );          
	  help.add( new HelpAboutAction(this));    
	  menuBar.add( help );    
    
    Vector frames = new Vector();
    frames.add( this );
    return( menuBar );

  } // createMenuBar
  	  	
  // Create the toolbar for this frame.
  protected JToolBar createToolBar() 
  {
    final JToolBar toolBar = new JToolBar();
    toolBar.setFloatable( true );
		toolBar.setBorderPainted(true);

    toolBar.add( new FileNewAction(this,"") );
    toolBar.add( new OpenInternalFrameAction(""));
    
    //toolBar.setOrientation(VERTICAL);

    return( toolBar );
  } // createToolBar
  	  
	// Methods to create our internal frames
  public void addPageFrame(String name) 
  {
  	if (pf == null)
  	{
	    pf = new PageFrame(name, this);
	    popups.addElement(pf);
	    desktop.add(pf, new Integer(1));
	    pf.setIconifiable(true);
  	}
  }
  
	// Methods to create our parameter frames
  public void addParameterFrame(String name) 
  {  	  		  		
  	if (ipf == null)
  	{  				  	
	    ipf = new ImageParameterFrame(name, this);
	    
			//JOptionPane.showMessageDialog( null , "step 1" );				    	
	    
	    //popups.addElement(ipf);
	    //desktop.add(ipf, new Integer(1));
	    //ipf.setIconifiable(true);
  	}
  	
    if (ipf.isValid())
    	ipf.show();  	

		/*
		if (ipf.isValid())
			System.out.println("parameter dialog valid is TRUE ");
		else			
			System.out.println("parameter dialog valid is FALSE ");  	  						  		
		*/

		if (ipf.isValid() == false)
  	{
			//System.out.println("parameter dialog is not valid");
			JOptionPane.showMessageDialog( null , "parameter dialog is not valid, trying again" );				    
			ipf = new ImageParameterFrame(name, this);
	    if (ipf.isValid())
	    	ipf.show();			
  	}
  }
  
	// Methods to create our color control frame
  public void addColorControlFrame(String name) 
  {  	  		  		
  	if (ccf == null)
  	{  				  	
	    ccf = new ColorControlFrame(name, this);
	    //popups.addElement(ccf);
	    //desktop.add(ccf, new Integer(1));
	    //ccf.setIconifiable(true);
  	}
  	
    if (ccf.isValid())
    	ccf.show();  	  	  		
    	
		if (ccf.isValid() == false)
  	{
			JOptionPane.showMessageDialog( null , "color dialog is not valid, trying again" );			    	
	    ccf = new ColorControlFrame(name, this);
	    if (ccf.isValid())
	    	ccf.show();
  	}
  }
    
  /*
	///////////////////////////////////////////////////////  
  public JInternalFrame getCurrentFrame() 
  {
		JInternalFrame currentFrame;
    for (int i = 0; i < popups.size(); i++) 
    {
      JInternalFrame currentFrame = (JInternalFrame)popups.elementAt(i);
      if (currentFrame.isSelected()) 
      {
				return currentFrame;
      }
    }
    return null;
  }
  */
  
	///////////////////////////////////////////////////////  
	// Main  
  public static void main(String args[]) 
  {
    DesktopManager mgr = new DesktopManager();
    mgr.setVisible(true);
  }  
  
	//////////
	/////////////////////////////////////////////////////////////
	//// open action initialization
  // An Action that creates an internal frame with an opened image
  public class OpenInternalFrameAction extends AbstractAction 
  {        
    // OpenInternalFrameAction, constructor.
    // Set the name and icon for this action.         
    public OpenInternalFrameAction(String text) 
    {
      super( text, new ImageIcon( "emberj/images/open.gif" ) );  // Open
    }
    
    // Perform the action, create an internal frame and
    // add it to the desktop pane.
    public void actionPerformed( ActionEvent e ) 
    {
      JInternalFrame internalFrame = createOpenInternalFrame();
      desktop.add( internalFrame, JLayeredPane.DEFAULT_LAYER );
    }
  }
  
  // Create an "OPEN" internal frame.
  public JInternalFrame createOpenInternalFrame() 
 	{      		 		 		
		JInternalFrame internalFrame = new JInternalFrame( "Internal JLabel" );
		
    JFileChooser chooser = new JFileChooser();
    
    ExampleFileFilter filter = new ExampleFileFilter(new String[] 
    		{"jpg", "gif"}, "JPEG and GIF Image Files");
    
    ExampleFileView fileView = new ExampleFileView();
    fileView.putIcon("jpg", jpgIcon);
    fileView.putIcon("gif", gifIcon);

    chooser.setFileView(fileView);
    chooser.addChoosableFileFilter(filter);
    chooser.setFileFilter(filter);

		FilePreviewer accessory = new FilePreviewer(chooser);
		accessory.setBorder(BorderFactory.createLoweredBevelBorder());
    chooser.setAccessory(accessory);   

    File swingFile = new File("emberj/images/swing-64.gif");
    if(swingFile.exists()) 
    {
			chooser.setCurrentDirectory(swingFile);
			chooser.setSelectedFile(swingFile);
		} 

    //int retval = chooser.showOpenDialog(MDIFrame.this);
    int retval = chooser.showOpenDialog(this);    
    				
		switch(retval)
		{
			case JFileChooser.APPROVE_OPTION:				
												
				String fileName = chooser.getSelectedFile().getAbsolutePath();
				//JOptionPane.showMessageDialog( null , "opening file: " +  fileName);				
				
				Icon icon = new ImageIcon(fileName);						
				
				JLabel lab = new JLabel(icon);		
				JScrollPane scrollPane = new JScrollPane(lab);				    

				// open the file				
		    internalFrame.getContentPane().add( scrollPane, BorderLayout.CENTER);
		    internalFrame.setResizable( true );
		    internalFrame.setClosable( true );
		    internalFrame.setIconifiable( true );
		    internalFrame.setMaximizable( true );      
		    internalFrame.setPreferredSize( new Dimension( 400, 300 ));            
		    internalFrame.setTitle(fileName);
		    internalFrame.pack();								
				
				break;
			case JFileChooser.CANCEL_OPTION:				
				//JOptionPane.showMessageDialog( null , "canceled operation" );				
				break;
				
			default:
				// closed without hitting a button
				//JOptionPane.showMessageDialog( null , "aborted operation" );				
				break;
		}
 		
		///////////// 		
		return( internalFrame );
  }

	//////////////////////////////////////////////////////////////////////////       	
  class FilePreviewer extends JComponent implements PropertyChangeListener 
  {
    ImageIcon thumbnail = null;
    File f = null;
    
    public FilePreviewer(JFileChooser fc) 
    {
		  setPreferredSize(new Dimension(400, 300));
		  fc.addPropertyChangeListener(this);
    }
      
    public void loadImage() 
    {
		  if(f != null)
		  {
	      ImageIcon tmpIcon = new ImageIcon(f.getPath());
	      if(tmpIcon.getIconWidth() >= 320) 
	      {
				  thumbnail = new ImageIcon(
  		      tmpIcon.getImage().getScaledInstance(320, 240, Image.SCALE_DEFAULT));		      
	      }
	      else
	     	{
		  		thumbnail = tmpIcon;
	      }
		  }
	  }
      
    public void propertyChange(PropertyChangeEvent e) 
    {
		  String prop = e.getPropertyName();
		  if(prop == JFileChooser.SELECTED_FILE_CHANGED_PROPERTY) 
		  {
	      f = (File) e.getNewValue();
	      if(isShowing()) 
	      {
		  		loadImage();
		  		repaint();
	    	}
	  	}
  	}

		public void update(Graphics g) 
  	{
  		paint(g);	
  	}

    public void paint(Graphics g) 
    {
    	// Don't put a "ShowMessage" in "Paint"!!
			//JOptionPane.showMessageDialog( null , "MDIFrame paint" );				    	
    	
		  if(thumbnail == null) 
		  {
    	  loadImage();
	  	}
	  	if(thumbnail != null) 
	  	{
	      int x = getWidth()/2 - thumbnail.getIconWidth()/2;
	      int y = getHeight()/2 - thumbnail.getIconHeight()/2;
	      if(y < 0)
		    {
			    y = 0;
		    }
		      
		    if(x < 5) 
		    {
			    x = 5;
		    }
	      thumbnail.paintIcon(this, g, x, y);
		  }
  	}
  }

  /** Image loading **/
  void loadImages() 
  {
    jpgIcon  = loadImageIcon("emberj/images/jpgIcon.jpg", "An icon that represents jpg images");
    gifIcon  = loadImageIcon("emberj/images/gifIcon.gif", "An icon that represents gif images");
    exitIcon = loadImageIcon("emberj/images/exit.gif", "An icon that represents exiting the application");
    
    //Image bullseye = new ImageIcon( "images/earth.gif" ).getImage();
    //setIconImage( bullseye );        
  }

	///////		
  public ImageIcon loadImageIcon(String filename, String description) 
  {
      return new ImageIcon(filename, description);
  }	   	      
}