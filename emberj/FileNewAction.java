/////////////////////////
// FileNewAction

package emberj;

import java.awt.event.ActionEvent;
import javax.swing.*;

public class FileNewAction extends AbstractAction 
{
  DesktopManager parent;
  
  public FileNewAction(DesktopManager dm, String text) 
  {
    super(text, new ImageIcon("emberj/images/new.gif"));
    parent = dm;
  }

  public void actionPerformed(ActionEvent ae) 
  {
		//JOptionPane.showMessageDialog( null , "New, ..." );
		parent.addPageFrame("");
  }
}

