/////////////////////////////////////////////////
// QSortAlgorithm.java

package emberj;

public class QSortAlgorithm
{    
	void quickSort(double a[], int lo0, int hi0) 
	{
    // Based on the QuickSort method by James Gosling from Sun's SortDemo applet
   
    int lo = lo0;
    int hi = hi0;
    double mid, t;

    if ( hi0 > lo0) 
    {
      mid = a[ ( lo0 + hi0 ) / 2 ];
      while( lo <= hi ) 
      {
        while( ( lo < hi0 ) && ( a[lo] < mid ) )
             ++lo;
        while( ( hi > lo0 ) && ( a[hi] > mid ) )
             --hi;
        if( lo <= hi ) 
       	{
       		// swap
	        t = a[lo]; 
	        a[lo] = a[hi];
	        a[hi] = t;
          ++lo;
          --hi;
        }
      }

      if( lo0 < hi )
        quickSort( a, lo0, hi );
      if( lo < hi0 )
        quickSort( a, lo, hi0 );
    }
  }  
  
  public void sort(double a[]) throws Exception
  {
    quickSort(a, 0, a.length - 1);
  }
}
