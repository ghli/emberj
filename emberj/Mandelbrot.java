///////////////////////////////////////////////////////////
//
// Developed by Stephen C. Ferguson
// First started on 3-30-99
// Last updated on 5-4-99
// http://home.hiwaay.net/~stephenf
//
///////////////////////////////////////////////////////////

package emberj;

import java.awt.*;
import java.applet.*;
import java.awt.image.MemoryImageSource;
import java.awt.event.*;
import java.lang.Math.*;
import javax.swing.*;

public class Mandelbrot extends JComponent implements Runnable
{		
  Image image;
  MemoryImageSource source; 

  int row, col;

  int i, j, index;
    
  int pixels[];

  int the_size;
  int value;

  boolean startCalculate;
  Thread thread;

  Timer timer;
	  
	final double zerotol  	= 1e-24;
	final double overflow   = 1e24;

  final int code  			= 0xFF000000;
	
  final double constPI  = 3.14159265359;
  final double const2PI = 6.28318530718;
		
	int Width;
	int Height;
	
  JInternalFrame parent;  
  DesktopManager dm;
	  
	// Complex z, c;		
	//////////
	// variables

	double cr, ci, zr, zi;
  double tzr, tzi;
  double zr1, zi1;
	
	double CRMIN, CRMAX, CRMID, CIMIN, CIMAX, CIMID;
	double CRMIN_NEW, CRMAX_NEW, CRMID_NEW, CIMIN_NEW, CIMAX_NEW, CIMID_NEW;
  double CRMID_OLD, CIMID_OLD;
  double CRMID_JUL, CIMID_JUL;
  double dMag_new, dMagnification;
  double Radius_x, Radius_y;
  double rx_ratio, ry_ratio;
  double xtot, ytot;  
  double xsav, ysav;  
  double dFactor3, dFactor4, dFactor5;  
  double dStrands;
  double r, g, b;  
  double dRedStep, dBluStep, dGrnStep;
  double dRedStart, dBluStart, dGrnStart;
  double temp, ldx, ldy;
  double xO, yO, xI, yI;
  double zsqr0, zsqr1, zsqrX, zsqrY;
  double minX, minY;
  double zsqrCX, zsqrCY;

  boolean Abort_Draw;
  boolean bZoomingIn;

	double Bailout;
  int T1, px, py;
  int red, grn, blu;
  int Maxit;  
  int formula, render, coloring;
  int render03_option;
  int jul, jul_save;
  int inverse, nRotation;
  int Iteration;
  int color;  	
  int tmp;
  int redx, grnx, blux;
  int redy, grny, bluy;
		
	//////////
	Complex_math cm;	

	FormulaComponent 		fc;
	RenderingComponent 	rc;
	ColoringComponent  	cc;
		
	// constructor  
  public Mandelbrot(JInternalFrame PF, DesktopManager DM)
	{
		//this.v = new Vars();		
		parent = PF;		
		dm = DM;
		timer = null;
 	}
    
  public void init()
  {      	  	    
  	// initialize some variables  	
    dRedStep = 5;
    dGrnStep = 5;
    dBluStep = 5;

    dRedStart = 0;
    dGrnStart = 0;
    dBluStart = 0;

    Maxit = 50;
    Bailout = 1E24;
    dStrands = 0.15;

    formula   = 3;
    render    = 1;
    coloring 	= 1;

    render03_option = 4;

    dFactor3 = 1;
    dFactor4 = 1;
    dFactor5 = 1;

    jul = 0;
    jul_save = 0;
    inverse = 0;

    nRotation = 1;		
		bZoomingIn = true;
		
    ResetCoordinates();
  }

	public void ResetCoordinates()
	{		
	  dMagnification = 0.1;
	  dMag_new = 0.1;
	
	  CRMIN = -4;
	  CRMAX =  4;
	  CIMIN = -4;
	  CIMAX =  4;
	
	  CRMIN_NEW = -4;
	  CRMAX_NEW =  4;
	  CIMIN_NEW = -4;
	  CIMAX_NEW =  4;
	
	  CRMID = ((CRMAX - CRMIN) / 2.0) + CRMIN;
	  CIMID = ((CIMAX - CIMIN) / 2.0) + CIMIN;						
	}  
      
  public void start()
  {
  	// create the image memory space
    Width  = dm.m_width;
    Height = dm.m_height;		    

    pixels     = new int[Width*Height];
    //iteration  = new int[Width*Height];
        
    // initialize pixel array
    for (i=0; i<Width*Height; i+=1)
      pixels[i] = 0xff0000ff;
    
    source = new MemoryImageSource(Width, Height, pixels, 0, Width);	  	  
    image = createImage(source);               		

		draw_the_image();
  } 
  
  public void image_resize()
	{
	  parent.setTitle("Resizing, ...");    
	  parent.show();    

  	// create the image memory space
    Width  = dm.m_width;
    Height = dm.m_height;		    
		
    pixels = new int[Width*Height];
        
    // initialize pixel array
    for (i=0; i<Width*Height; i+=1)
      pixels[i] = 0xff0000ff;
    
    source = new MemoryImageSource(Width, Height, pixels, 0, Width);	  	  
    image = createImage(source);               		
		
		dm.pf.contentPane.remove( dm.pf.scrollPane);    		
		dm.pf.set_image_container();

		draw_the_image();
	}
 
 	public void draw_the_image()
	{		        
	  parent.setTitle("Busy, ...");    
		
  	if(thread == null) 
    {      	
			//JOptionPane.showMessageDialog( null , "jitters start" );				    	    	
    	
      thread = new Thread(this);
      thread.start();
      
	    /////// create periodic timer
	    timer = new Timer(1000, new ActionListener()
			{
				public void actionPerformed( ActionEvent event)
				{
          thread_manager();          			    	

					if (thread != null && Width*Height <= 800*600)
			    	image.flush();					
					
				  //JOptionPane.showMessageDialog( null , "timer" );								
					if (parent.isSelected())
					{
					  parent.show();
					}
					else
					  parent.updateUI();
					  					
					//System.out.println("index = " + index);
				}						
			});		
	
			timer.start();      
			String title = "EmberJ - formula=" + formula + ", coloring=" + coloring + ", render=" + render + ", sub-render=" + render03_option;
			dm.setTitle(title);
			
    }        
  }

  private void AdjustCoords()
	{		
	  Radius_x = 2/dMagnification;
	  Radius_y = 2/dMagnification;
	
	  CRMAX = CRMID + Radius_x;
	  CRMIN = CRMID - Radius_x;
	  CIMAX = CIMID + Radius_y;
	  CIMIN = CIMID - Radius_y;
	
	  rx_ratio = (double)Width/(double)Height;
	  ry_ratio = (double)Height/(double)Width;
	
	  rx_ratio = rx_ratio + (1-rx_ratio)/2;
	  ry_ratio = ry_ratio + (1-ry_ratio)/2;
	
	  CRMAX = CRMAX + (Radius_x * (rx_ratio) - Radius_x);
	  CRMIN = CRMIN - (Radius_x * (rx_ratio) - Radius_x);
	  CIMAX = CIMAX + (Radius_x * (ry_ratio) - Radius_y);
	  CIMIN = CIMIN - (Radius_x * (ry_ratio) - Radius_y);				
	}      
      
  public void stop()
  {  		
		//JOptionPane.showMessageDialog( null , "Mandelbrot stop." );
  	
    thread = null;
    //image = null;
		timer.stop();    
  }

  private void thread_manager()
  {    
  	if (image != null)
  	{
	    //image.flush();
			//System.out.println("flush index = " + index);	    
    } 

		try 
    {
      if (startCalculate == true)
      {                                    	      	
			  parent.setTitle("Row: " + String.valueOf(Height-row));
      }
		  thread.sleep(10);
		} 
    catch( InterruptedException e ) 
    {
			System.out.println("Interrupted exception in thread sleep");	    
		  //JOptionPane.showMessageDialog( null , "Interrupted exception" );				    	
		}     
  }
  
  public void run() 
  {
	  thread = Thread.currentThread();
	  thread.setPriority(Thread.MIN_PRIORITY);
	  	  
	  // Initialize formula class
		String formula_param = null;

	  // Initialize coloring class
		String coloring_param = null;

		switch (formula)
  	{  		
  		case 01:
				formula_param = "emberj.formula01";
		  	break;
		  	
  		case 02:
				formula_param = "emberj.formula02";
		  	break;

  		case 03:
				formula_param = "emberj.formula03";
		  	break;
		  			  	
		  default:
			  JOptionPane.showMessageDialog( null , "formula method not yet implemented, ..." );    	
		  	break;		  			  	
  	}
  	
  	fc = FormulaComponent.create(formula_param);
    if (fc == null)
		  JOptionPane.showMessageDialog( null , "classname problem" );    	
    else
      fc.init(this);
    	  	  	  	    		
		switch (coloring)
  	{  		
  		case 01:
				coloring_param = "emberj.coloring01";
		  	break;

  		case 02:
				coloring_param = "emberj.coloring02";
		  	break;
		  	
  		case 03:
				coloring_param = "emberj.coloring03";
		  	break;
		  	
		  default:
			  JOptionPane.showMessageDialog( null , "coloring method not yet implemented, ..." );    	
		  	break;		  			  	
  	}
  	
  	cc = ColoringComponent.create(coloring_param);
    if (cc == null)
		  JOptionPane.showMessageDialog( null , "classname problem" );    	
    else
      cc.init(this);
    	  	  	  	  
	  // Initialize render class
		String render_param = null;
	
		switch (render)
  	{  		
  		case 01:
				render_param = "emberj.render01";
		  	break;
		  	
  		case 02:
				render_param = "emberj.render02";
		  	break;
		  	
  		case 03:
				render_param = "emberj.render03";
		  	break;
		  	
		  default:
			  JOptionPane.showMessageDialog( null , "render method not yet implemented, ..." );    	
		  	break;		  			  	
  	}
  	
  	rc = RenderingComponent.create(render_param);
    if (rc == null)
		  JOptionPane.showMessageDialog( null , "classname problem" );    	
    else
      rc.init(this);
    	  
		// initialize image				
		
		// m = new Complex_math(zr, zi, cr, ci);		
    		
    index = 0;
    row = 0;                
    col = 0;
    
  	AdjustCoords();  	  	
  	
	  xO = CRMIN;
	  yO = CIMIN;
	  xI = CRMAX;
	  yI = CIMAX;
	
	  ldx = (xI - xO) / (double) Width;             // compute size of pixels
	  ldy = (yI - yO) / (double) Height;   
	  	  
		cm = new Complex_math();
	  	  
    startCalculate = true;	  
	  while (startCalculate == true) 
    {          	    	    	
      if (startCalculate == true)
      {                                    	      	
        Calculate();          
        row += 1;                  

        if (row % 10 == 0)
					thread_manager();        	

        if (index >= Width*Height)
        {
          startCalculate = false;
          index = 0;
        }  
      }  
    }
    
	  parent.setTitle("Flushing, ...");    
	  image.flush();
    thread_manager();
    
	  parent.setTitle("Ready");    
	  parent.show();    
    stop(); 
  }

  private void Calculate()
  {  		  	  
    for (col=0; col<Width; col+=1) 
    {  	
	  	// Initialize pixel
	    xsav = 0;
	    ysav = 0;
	    xtot = 0;
	    ytot = 0;
	
	    Iteration = 0;
	    
	    px = col;
	    py = row;
	
	    zr = xO + px * ldx;  	    // start julia on pixel
	    zi = yO + py * ldy;
	
	    if (jul == 0)    	        // Start mandelbrot set on 0,0
	  	{
	      cr = zr;
	      ci = zi;	              // and use pixel for c.
	  	}
	    else 
	    if (jul_save == 0)  			// If previous was a mandelbrot
	  	{
	      cr = CRMID_JUL;
	      ci = CIMID_JUL;  				// A Julia coordinate
	  	}	
	  	  							
			//minX = zr*zr;
			//minY = zi*zi;
			
			//zsqrCX = Math.sin(cr*cr);
			//zsqrCY = Math.sin(ci*ci);						

			//zr = 1;
			//zi = 0;
			
			fc.init(this);
			
			//cm = new Complex_math(zr, zi, cr, ci);
			
			cm.zr = zr;
			cm.zi = zi;			
			cm.cr = cr;
			cm.ci = ci;
			
      //zsqr0 = cm.z_sum_of_squares();

			//zsqr1 = zr*zr+zi*zi;
			
			//cm.zr = 0.0;
			//cm.zi = 0.0;
			
      for (Iteration=0; Iteration<Maxit; Iteration+=1) 
      {       	      	
      	zr1 = cm.zr;
      	zi1 = cm.zi;          	
      	
      	fc.formula();  
      	      	
				//zsqr1 = cm.z_sum_of_squares();			
      	      	      	
      	zr = cm.zr;
      	zi = cm.zi;

      	//if (m.z_sum_of_squares() < zerot)
          //break;        
          
        if (rc.filter())
        	break;				      		
        	
				//zsqr0 = zsqr1;
      		
      }
      
      //i = (int) ((double) Iteration * (768 / (double) Maxit));
      
      //i = (int) ((double) Iteration * (256 / (double) Maxit));
            
      //i = (int) (200 * Math.abs(zsqrX - zsqrY));
      //i = (int) (2500 * Math.abs(minX - minY));

			//zsqrX = zsqrX+minX;
			//zsqrY = zsqrY+minY;
				
			//zsqrX = ((double)Iteration/(double)Maxit+zsqrX/100)/2;
			//zsqrY = ((double)Iteration/(double)Maxit+zsqrY/100)/2;
			
			rc.post_filter();

			cc.coloring();
      
    }
  }

	/*
  private void coloring_01()
  {
  }    
  
  private void coloring_02()
  {
  }    
               
  private void coloring_03()
  {
  }    
  */

	///////////////////////////////////////////////
	//////////// zooming / panning operations
	public void mouse_released(int X_Pixel, int Y_Pixel, int releasedX, int releasedY)
	{
		double SelectionWidth, SelectionHeight;
		double SelectionLeft, SelectionTop;
		double rb_center_x, rb_center_y, rb_avg;
		double dim_width, dim_height, dim_avg;
		double sxmin, sxmax, symin, symax, x_size, y_size;
		
		// X_Pixel, Y_Pixel is mouse down coordinates
		//
	  // Redraw the tracking rectangle
	  // Transform to display coordinates
	  if (bZoomingIn)
	  { 
	    SelectionWidth  = ((double) dm.m_width * 0.5);
	    SelectionHeight = ((double) dm.m_height*0.5);
	  }
	  else // Zooming out
		{
	    SelectionWidth  = dm.m_width*2;
	    SelectionHeight = dm.m_height*2;
		}
	
	  rb_center_x = X_Pixel;
	  rb_center_y = Y_Pixel;
	
	  // calculate the average width & height
	  rb_avg = (SelectionWidth + SelectionHeight) / 2;
	
	  dim_width  =  Width;
	  dim_height =  Height;
	  dim_avg    = (Width + Height) / 2;

	  // Calculate transformed points
	  SelectionWidth  = (rb_avg * (dim_width / dim_avg));
	  SelectionLeft   = (rb_center_x - rb_avg * (dim_width / dim_avg) / 2.0);
	  SelectionHeight = (rb_avg * (dim_height / dim_avg));
	  SelectionTop    = (rb_center_y - rb_avg * (dim_height / dim_avg) / 2.0);
	
	  // scale the screen coordinates
	  sxmin = SelectionLeft / dim_width;
	  sxmax = (SelectionLeft + SelectionWidth) / dim_width;
	
	  symin = SelectionTop / dim_height;
	  symax = (SelectionTop + SelectionHeight) / dim_height;
	
	  x_size = CRMAX - CRMIN;
	  y_size = CIMAX - CIMIN;
	
	  CRMIN_NEW = x_size * sxmin + CRMIN;
	  CRMAX_NEW = x_size * sxmax + CRMIN;
	
	  CIMIN_NEW = y_size * symin + CIMIN;
	  CIMAX_NEW = y_size * symax + CIMIN;
	
	  CRMID_OLD = CRMID;
	  CIMID_OLD = CIMID;
	
	  CRMID_NEW = ((CRMAX_NEW - CRMIN_NEW) / 2.0) + CRMIN_NEW;
	  CIMID_NEW = ((CIMAX_NEW - CIMIN_NEW) / 2.0) + CIMIN_NEW;
	
	  // Calculate the Magnification (2 / average of width & length)
	  dMag_new = ((Math.abs(CRMAX_NEW - CRMIN_NEW) / 2)
	           + (Math.abs(CIMAX_NEW - CIMIN_NEW) / 2)) / 2;
	
	  if (dMag_new != 0)
	    dMag_new = (1 / dMag_new) * 2;

		////////////////////////////////////////////////////
		// Zoom1Click

    CRMIN = CRMIN_NEW;
    CRMAX = CRMAX_NEW;
    CIMIN = CIMIN_NEW;
    CIMAX = CIMAX_NEW;

    CRMID = ((CRMAX - CRMIN) / 2.0) + CRMIN;
    CIMID = ((CIMAX - CIMIN) / 2.0) + CIMIN;

    dMagnification = dMag_new;
    
    draw_the_image();
	    
	} // end mouse_released		  					
  
}



	
	