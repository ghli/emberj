///////////////////////////////////////////////////
// ImageParameterFrame.java
// A simple extension of the JInternalFrame class
 
package emberj;

import java.awt.*;
import java.awt.event.*;
import java.lang.*;
import javax.swing.*;

//public class ImageParameterFrame extends JInternalFrame
public class ImageParameterFrame extends JDialog
{
  DesktopManager dm;
  int width, height;

  JLabel textLabelWidth, textLabelHeight;
  JLabel textLabelMaxit;

  JLabel textLabelrStart;
  JLabel textLabelgStart;
  JLabel textLabelbStart;
  
  JLabel textLabelrStep;
  JLabel textLabelgStep;
  JLabel textLabelbStep;

  JSeparator textLabelSeparator;
  JLabel textLabelBailout;

  JTextField textFieldWidth, textFieldHeight;
  JTextField textFieldMaxit;  

  //JTextField textFieldrStart;  
  //JTextField textFieldgStart;  
  //JTextField textFieldbStart;  

  JTextField textFieldrStep;  
  JTextField textFieldgStep;  
  JTextField textFieldbStep;  

  JTextField textFieldBailout;  

  public ImageParameterFrame(String name, DesktopManager DM) 
  {
    //super(name, true, false, true, true);
    super(DM, false);
    dm = DM;
    setBounds(360, 100, 190, 300);

    Container contentPane = getContentPane();    
    
		//////////////////////////////////////////////////////////////
		// set up the parameter frame menu bar and actions
    //JMenuBar jmb = new JMenuBar();
    //JMenu fileMenu = new JMenu("Close");

    //Action disposeAction = new DisposeParameterFrameAction(dm, this);        
    
    //fileMenu.add(disposeAction);
    //jmb.add(fileMenu);

    //setJMenuBar(jmb);        
        
		//////////////////////////////////////////////////////////////
		// Create a panel assign grid layout with X rows, Y columns
		JPanel panelNorth = new JPanel(new GridLayout(7, 2));
		panelNorth.setBorder( BorderFactory.createEtchedBorder());
		
		JPanel panelSouth = new JPanel(new GridLayout(1, 2));
		panelSouth.setBorder( BorderFactory.createEtchedBorder());
		
    width 	= dm.m_width;
    height  = dm.m_height;
		
		///////////////////////////////////////////////////////////////
		// add the Width text label	
		textLabelWidth = new JLabel("");			  		 
		textLabelWidth.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelWidth);			
		
		textFieldWidth = new JTextField(String.valueOf(width), 7);
		panelNorth.add(textFieldWidth);			

		// add the Height text label	
		textLabelHeight = new JLabel("");
		textLabelHeight.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelHeight);

		textFieldHeight = new JTextField(String.valueOf(height), 7);
		panelNorth.add(textFieldHeight);			

		// add the Maxit text label	
		textLabelMaxit = new JLabel("");
		textLabelMaxit.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelMaxit);

		textFieldMaxit = new JTextField(String.valueOf(dm.pf.Mandelbrot.Maxit), 7);
		panelNorth.add(textFieldMaxit);			
		
		// add a separator
		//textLabelSeparator = new JSeparator();
		//textLabelSeparator.setBorder( BorderFactory.createEtchedBorder());  		 
		//panelNorth.add(textLabelSeparator);		
		//panelNorth.add(textLabelSeparator);		

		// add the rStep text label	
		textLabelrStep = new JLabel("");
		textLabelrStep.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelrStep);

		textFieldrStep = new JTextField(String.valueOf(dm.pf.Mandelbrot.dRedStep), 7);
		panelNorth.add(textFieldrStep);			

		// add the gStep text label	
		textLabelgStep = new JLabel("");
		textLabelgStep.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelgStep);

		textFieldgStep = new JTextField(String.valueOf(dm.pf.Mandelbrot.dGrnStep), 7);
		panelNorth.add(textFieldgStep);			

		// add the bStep text label	
		textLabelbStep = new JLabel("");
		textLabelbStep.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelbStep);

		textFieldbStep = new JTextField(String.valueOf(dm.pf.Mandelbrot.dBluStep), 7);
		panelNorth.add(textFieldbStep);			
		
		// add a separator
		//panelNorth.add(textLabelSeparator);
		
		// add the Bailout text label	
		textLabelBailout = new JLabel("");
		textLabelBailout.setBorder( BorderFactory.createEtchedBorder());  		 
		panelNorth.add(textLabelBailout);

		textFieldBailout = new JTextField(String.valueOf(dm.pf.Mandelbrot.Bailout), 7);
		panelNorth.add(textFieldBailout);			

		///////////////////////////////////////////////////////////////
		// add the Apply button		
		JButton applyButton = new JButton("Apply");
		applyButton.setBorder( BorderFactory.createEtchedBorder());  		 
		applyButton.addActionListener(new ApplyButtonAction(dm, this));
		panelSouth.add(applyButton);
				
		contentPane.add( panelNorth, BorderLayout.NORTH);    
		contentPane.add( panelSouth, BorderLayout.SOUTH);    
		
		textLabelUpdate();		
		
		this.show();
  } 
  
  public void textLabelUpdate()
	{		
    width 	= dm.m_width;
    height  = dm.m_height;

    textLabelWidth.setText(" width: " + String.valueOf(width));            
    textLabelHeight.setText("height: " + String.valueOf(height));        				
    textLabelMaxit.setText("Maxit: " + String.valueOf(dm.pf.Mandelbrot.Maxit));        				

    textLabelrStep.setText("rStep: " + String.valueOf(dm.pf.Mandelbrot.dRedStep));   	
    textLabelgStep.setText("gStep: " + String.valueOf(dm.pf.Mandelbrot.dGrnStep));   	
    textLabelbStep.setText("bStep: " + String.valueOf(dm.pf.Mandelbrot.dBluStep));   	

    textLabelBailout.setText("Bailout: " + String.valueOf(dm.pf.Mandelbrot.Bailout));   	
    
	}  
}