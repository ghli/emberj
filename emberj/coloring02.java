/////////////////////////
// coloring02

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class coloring02 extends ColoringComponent
{
	Mandelbrot m;
	double r, g, b;
	int red, grn, blu, i, tmp;
	
	public coloring02()		
	{
		//JOptionPane.showMessageDialog( null , "coloring02 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "coloring02 init, ..." );	
	}

	public void coloring()
	{
		//if (m.render == 1)
		//{
		  //i = m.Iteration*(768/m.Maxit);
		  //i = m.Iteration;
		//}
		//else
		{
	  	//m.xtot = m.xtot-1;
		  //m.ytot = m.ytot-1;
				
  		i = (int) (((Math.abs(m.xtot)+Math.abs(m.ytot))/2)*767);
  		//i = (int) Math.round(360*(m.xtot + m.ytot));
		}
  	
    if (i < 256)
      red = i;        
    else
      red = 255;
      
    if (i < 512 && i > 255)
    {
      grn = i - 256;
    }
    else
    {
      if (i >= 512)
        grn = 255;
      else
        grn = 0;
    }
                
    if (i < 768 && i > 511)
      blu = i - 512;
    else
  	{
  		if (i >= 768)
      	blu = 255;        
      else
      	blu = 0;
  	}
      
    if ((red & 0x1FF) > 0xFF)
    {
    	red = red & 0xFF;    	// mask the color
      red = ~ red;        	// Invert the color
    }

    if ((grn & 0x1FF) > 0xFF)
    {
    	grn = grn & 0xFF;    	// mask the color
      grn = ~ grn;        	// Invert the color
    }

    if ((blu & 0x1FF) > 0xFF)
    {
    	blu = blu & 0xFF;     	// mask the color
      blu = ~ blu;         	// Invert the color
    }

		/*
    // try swapping colors around
    tmp = red;
    red = grn;
    grn = blu;
    blu = tmp;
    */

		//grn = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((grn + red + blu)/3)));			
		//red = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((red + blu + grn)/3)));
		//blu = (int) ((2-1/(0.5+Math.abs(zsqrX + zsqrY))*(double)Math.abs((blu + red + grn)/3)));			

		//grn = Math.abs((grn + red + blu)/3);			
		//red = Math.abs((red + blu + grn)/3);
		//blu = Math.abs((blu + red + grn)/3);			

		/*
		if (xtot > ytot)
  	{
  		// invert the color
  		red = ~red;
  		grn = ~grn;
  		blu = ~blu;
  	}
  	*/
    
    ////////////////////    
    
	  red = red & 0xFF;
	  grn = grn & 0xFF;
	  blu = blu & 0xFF;      	  	  

    red = (red << 16) & 0x00ff0000;  // shift left 16 places            
    grn = (grn << 8)  & 0x0000ff00;  // shift left 8 places
        
    m.color = red | grn | blu;
    
    m.pixels[m.index]    = m.code | m.color;    
    m.index+=1;

	}					
	
}