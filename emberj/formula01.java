/////////////////////////
// formula01

package emberj;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class formula01 extends FormulaComponent
{
	Mandelbrot m;
	
	public formula01()		
	{
		//JOptionPane.showMessageDialog( null , "formula01 constructor, ..." );
	}
	
	public void init(Mandelbrot M)
	{
		m = M;	
		//JOptionPane.showMessageDialog( null , "formula01 init, ..." );	
	}

	public void formula()
	{	
		// z=z*z+c;					
		
    m.tzr = m.cm.zr*m.cm.zr - m.cm.zi*m.cm.zi + m.cm.cr;
    m.tzi = 2*m.cm.zr*m.cm.zi + m.cm.ci;

    m.cm.zr = m.tzr;
    m.cm.zi = m.tzi;		
	}					
	
}





