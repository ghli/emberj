/////////////////////////
// HelpAboutAction

package emberj;

import java.awt.event.ActionEvent;
import javax.swing.*;

public class HelpAboutAction extends AbstractAction 
{
  DesktopManager parent;
  
  public HelpAboutAction(DesktopManager dm) 
  {
    //super("New", new ImageIcon("images/new.gif"));
    super("About");
    parent = dm;
  }

  public void actionPerformed(ActionEvent ae) 
  {
		JOptionPane.showMessageDialog( null , "EmberJ, beta v0.005, Developed by Stephen C. Ferguson, May 1999, http://home.hiwaay.net/~stephenf/");
		//parent.addPageFrame("new frame");
		
  }
}